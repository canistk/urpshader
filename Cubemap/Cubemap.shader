Shader "Kit/Universal Render Pipeline/Cubemap"
{
    Properties
    {
        [NoScaleOffset] _Cubemap ("Cube map Texture", Cube) = "grey" {}
        [IntRange] _LOD("Cubemap LOD", Range(0, 5)) = 0
        _Color("_Color (default = 1,1,1,1)", color) = (1,1,1,1)
        [header(Effect)]
        _Depth("View Depth (Default = 1)", Range(0.001,2.0)) = 1
        _ReflectWeight("Reflect weight", Range(-1.0,1.0)) = 0
        _FlipNormalWeight("Blend to surface normal(inverse)", Range(0.0, 1.0)) = 0
    }

    // The SubShader block containing the Shader code. 
    SubShader
    {
        // SubShader Tags define when and under which conditions a SubShader block or
        // a pass is executed.
        // https://docs.unity3d.com/Manual/SL-SubShaderTags.html
        Tags {
            "RenderType" = "Opaque"
            "Queue" = "Geometry" // Queue : { Background, Geometry, AlphaTest, Transparent, Overlay }
            "RenderPipeline" = "UniversalPipeline"
            // "DisableBatching" = "True"
        }
        LOD 100
		
        Pass
        {
            // The HLSL code block. Unity SRP uses the HLSL language.
            HLSLPROGRAM
            // This line defines the name of the vertex shader. 
            #pragma vertex vert
            // This line defines the name of the fragment shader. 
            #pragma fragment frag
            
            // due to using ddx() & ddy()
            #pragma target 3.0

            // The Core.hlsl file contains definitions of frequently used HLSL
            // macros and functions, and also contains #include references to other
            // HLSL files (for example, Common.hlsl, SpaceTransforms.hlsl, etc.).
            // https://github.com/Unity-Technologies/Graphics/blob/master/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            #include "../ShaderLibrary/ViewExtend.hlsl"

            TEXTURECUBE(_Cubemap); SAMPLER(sampler_Cubemap);
            UNITY_INSTANCING_BUFFER_START(UnityPerMaterial)
                UNITY_DEFINE_INSTANCED_PROP(half4, _Color);
                UNITY_DEFINE_INSTANCED_PROP(half, _Depth);
                UNITY_DEFINE_INSTANCED_PROP(half, _ReflectWeight);
                UNITY_DEFINE_INSTANCED_PROP(half, _FlipNormalWeight);
                UNITY_DEFINE_INSTANCED_PROP(half, _LOD);
            UNITY_INSTANCING_BUFFER_END(UnityPerMaterial)

            // The structure definition defines which variables it contains.
            // This example uses the Attributes structure as an input structure in
            // the vertex shader.
            struct Attributes
            {
                // The positionOS variable contains the vertex positions in object
                // space.
                half4   positionOS  : POSITION;
                half2   uv          : TEXCOORD0;
                half3   normalOS    : NORMAL;
                half4   tangentOS   : TANGENT;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                // The positions in this struct must have the SV_POSITION semantic.
                half4   positionCS  : SV_POSITION;
                half2   uv          : TEXCOORD0;
                half3   positionWS  : TEXCOORD1;
                half3   normalWS    : TEXCOORD2;
                half4   viewRayOS   : TEXCOORD3;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                UNITY_VERTEX_OUTPUT_STEREO
            };            

            // The vertex shader definition with properties defined in the Varyings 
            // structure. The type of the vert function must match the type (struct)
            // that it returns.
            v2f vert(Attributes IN)
            {
                // Declaring the output object (OUT) with the Varyings struct.
                v2f OUT;

                UNITY_SETUP_INSTANCE_ID(IN);
                //UNITY_INITIALIZE_OUTPUT(v2f, OUT); // Support in 2020.3
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT); // VR support - Single-Pass Stereo Rendering for Android
                UNITY_TRANSFER_INSTANCE_ID(IN, OUT); // necessary only if you want to access instanced properties in the fragment Shader.

                // VertexPositionInputs contains position in multiple spaces (world, view, homogeneous clip space, ndc)
                // Unity compiler will strip all unused references (say you don't use view space).
                // Therefore there is more flexibility at no additional cost with this struct.
                VertexPositionInputs vertexPositionInput = GetVertexPositionInputs(IN.positionOS.xyz);

                // The TransformObjectToHClip function transforms vertex positions
                // from object space to homogenous clip space.
                OUT.positionCS = vertexPositionInput.positionCS;
                OUT.positionWS = vertexPositionInput.positionWS;
                OUT.uv = IN.uv;

                OUT.viewRayOS = PackViewRayOS(vertexPositionInput.positionVS);

                VertexNormalInputs normalInput = GetVertexNormalInputs(IN.normalOS, IN.tangentOS);
                OUT.normalWS = normalInput.normalWS;

                return OUT;
            }

            // The fragment shader definition.            
            half4 frag(v2f IN) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID(IN); // necessary only if any instanced properties are going to be accessed in the fragment Shader.
                UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(IN); // VR support - Single-Pass Stereo Rendering for Android
                // Read GPU instancing data
                half4 color = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _Color);
                half depth = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _Depth);
                half reflectWeight = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _ReflectWeight);
                half flipNormalWeight = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _FlipNormalWeight);
                half lod = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _LOD);

                // [important note]
                //========================================================================
                // now do "viewRay z division" that we skipped in vertex shader earlier.
                half3 viewRayOS = UnpackViewRayOS(IN.viewRayOS) * depth;

                half3 _refract = refract(viewRayOS, -IN.normalWS, reflectWeight);
                half3 ray = lerp(_refract, -IN.normalWS, flipNormalWeight * 0.5);
                half4 sample = SAMPLE_TEXTURECUBE_LOD(_Cubemap, sampler_Cubemap, ray, lod);
                // Defining the color variable and returning it.
                half4 col = color * sample;
                return col;
            }
            ENDHLSL
        }
    }
}