// https://docs.unity3d.com/Packages/com.unity.render-pipelines.universal@11.0/manual/writing-shaders-urp-reconstruct-world-position.html
Shader "Kit/Universal Render Pipeline/Tools/CameraDepthTexture"
{
    Properties
    {
        [Toggle(_LinearEyeDepth)] _LinearEyeDepth("linearEyeDepth", float) = 0
        [Toggle(_Linear01DepthFromNear)] _Linear01DepthFromNear("Linear01DepthFromNear", float) = 0
    }

    SubShader
    {
        Tags {
            "RenderType" = "Opaque"
            "RenderPipeline" = "UniversalRenderPipeline"
            "Queue" = "Geometry+499"
            "ForceNoShadowCasting" = "True"
            "IgnoreProjector" = "True"
            "PreviewType" = "Plane"
            "DisableBatching" = "True"
        }
        LOD 100
        Cull Off

        Pass
        {
            Name "Overlay"
            Tags {
                "LightMode" = "SRPDefaultUnlit"
            }
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma shader_feature_local _LinearEyeDepth
            #pragma shader_feature_local _Linear01DepthFromNear
            // The Core.hlsl file contains definitions of frequently used HLSL
            // macros and functions, and also contains #include references to other
            // HLSL files (for example, Common.hlsl, SpaceTransforms.hlsl, etc.).
            // https://github.com/Unity-Technologies/Graphics/blob/master/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            // The DeclareDepthTexture.hlsl file contains utilities for sampling the
            // Camera depth texture.
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/DeclareDepthTexture.hlsl"


            CBUFFER_START(UnityPerMaterial)
            sampler2D _MainTex;
            float4 _MainTex_ST;
            CBUFFER_END
            struct Attributes
            {
                float4 positionOS   : POSITION;
                float2 uv           : TEXCOORD0;
            };
            struct Varyings
            {
                float4 positionCS  : SV_POSITION;
                float3 uv_fog       : TEXCOORD0;
                float3 positionVS   : TEXCOORD1;
                float3 positionWS   : TEXCOORD2;
                float4 positionNDC  : TEXCOORD3;
            };

            Varyings vert(Attributes IN)
            {
                Varyings OUT;
                VertexPositionInputs vertexPositionInput = GetVertexPositionInputs(IN.positionOS.xyz);
                OUT.positionCS = vertexPositionInput.positionCS; // Homogeneous clip space position (-1,-1) ~ (1, 1)
                OUT.positionVS = vertexPositionInput.positionVS; // View space position
                OUT.positionWS = vertexPositionInput.positionWS; // World space position
                OUT.positionNDC = vertexPositionInput.positionNDC; // Homogeneous normalized device coordinates (0,0) ~ (1,1)

                // regular unity fog
#if _UnityFogEnable
                OUT.uv_fog = float3(IN.uv, ComputeFogFactor(OUT.positionCS.z));
#else
                OUT.uv_fog = float3(IN.uv, 0);
#endif
                return OUT;
            }

            float4 frag(Varyings IN) : SV_Target
            {
                // To calculate the UV coordinates for sampling the depth buffer,
                // divide the pixel location by the render target resolution
                // _ScaledScreenParams.
                float2 screenUV = IN.positionCS.xy / _ScaledScreenParams.xy;

                // Sample the depth from the Camera depth texture.
                #if UNITY_REVERSED_Z
                    real depth = SampleSceneDepth(screenUV);
                #else
                    // Adjust Z to match NDC for OpenGL ([-1, 1])
                    real depth = lerp(UNITY_NEAR_CLIP_VALUE, 1, SampleSceneDepth(screenUV));
                #endif
                
                // Convert NDC point to ViewSpace(VC), in case we don't have positionVS
                // float cameraDistance = length(mul(unity_CameraInvProjection, IN.positionNDC).xyz);
                // float cameraDistance = length(mul(UNITY_MATRIX_I_P, IN.positionNDC).xyz);
                float cameraDistance = length(IN.positionVS.xyz);

#if _LinearEyeDepth
                float linearEyeDepth = LinearEyeDepth(depth, _ZBufferParams);
                float coc = saturate(cameraDistance / linearEyeDepth);
                return float4(coc,coc,coc,1);
#endif

#if _Linear01DepthFromNear
                float linear01DepthFromNear = Linear01DepthFromNear(depth, _ZBufferParams);
                float coc2 = linear01DepthFromNear;
                return float4(coc2,coc2,coc2,1);
#endif

                // The check board for depth texture from Unity example.
                // https://docs.unity3d.com/Packages/com.unity.render-pipelines.universal@11.0/manual/writing-shaders-urp-reconstruct-world-position.html
                // Reconstruct the world space positions.
                float3 worldPos = ComputeWorldSpacePosition(screenUV, depth, UNITY_MATRIX_I_VP);
                // return float4(1 - saturate(length(worldPos.xz)), 0, 0, 1);

                // The following part creates the checkerboard effect.
                // Scale is the inverse size of the squares.
                uint scale = 2;
                // Scale, mirror and snap the coordinates.
                uint3 worldIntPos = uint3(abs(worldPos.xyz * scale));
                // Divide the surface into squares. Calculate the color ID value.
                bool white = ((worldIntPos.x) & 1) ^ (worldIntPos.y & 1) ^ (worldIntPos.z & 1);
                // Color the square based on the ID value (black or white).
                half4 color = white ? half4(1,1,1,1) : half4(.1,.1,.1,1);

                // Set the color to black in the proximity to the far clipping
                // plane.
                #if UNITY_REVERSED_Z
                    // Case for platforms with REVERSED_Z, such as D3D.
                    if (depth < 0.0001)
                        return half4(0,0,0,1);
                #else
                    // Case for platforms without REVERSED_Z, such as OpenGL.
                    if (depth > 0.9999)
                        return half4(0,0,0,1);
                #endif
                return color;
            }
            ENDHLSL
        }
    }
}