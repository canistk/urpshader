using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit
{
    public static class RenderKit
    {
        public static Texture2D GenerateMipMap(int mipLevel = 12, bool log = false)
        {
            if (mipLevel < 1)
                mipLevel = 1;
            else if (mipLevel > 12)
            {
                // Level 1 = 2x2
                // Level 12 = 4096x4096
                Debug.LogWarning("Max mip level = 12, since 4096 is max texture size.");
                mipLevel = 12;
            }

            var sb  = log ? new System.Text.StringBuilder(200) : null;
            if (log)
            {
                sb.Append("Generate mipmap level = ").AppendLine(mipLevel.ToString());
            }
            var m2  = (int)Mathf.Pow(2, mipLevel);
            var tex = new Texture2D(m2, m2, TextureFormat.RGBA32, true, false);
            var freq = new Vector3(3f, 2f, 3f);
            var phase = new Vector3(4f, 2f, 1f);
            for (int mip = 0; mip < mipLevel; ++mip)
            {
                var pt      = (float)(mipLevel - mip) / (float)mipLevel;
                var color   = ColorExtend.GetSinColor(pt, freq, phase);
                var size    = (int)Mathf.Pow(2, mipLevel - mip);
                tex.FillColor(size, size, color, mip);

                if (log)
                    sb.AppendLine(ColorExtend.ToRichText(color, $"Mip level {mip.ToString().PadRight(2)}, {size.ToString().PadRight(4)}, {pt:F3}%"));
            }
            tex.requestedMipmapLevel    = mipLevel;
            tex.wrapMode                = TextureWrapMode.Repeat;
            tex.minimumMipmapLevel      = mipLevel;
            tex.filterMode              = FilterMode.Trilinear;

            if (log)
            {
                Debug.Log(sb.ToString());
            }
            // actually apply all SetPixels32, don't recalculate mip levels
            tex.Apply(false);
            return tex;
        }
    }
}