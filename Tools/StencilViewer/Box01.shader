Shader "Kit/Universal Render Pipeline/Tools/Stencil Box 01"
{
    Properties
    {
        // https://docs.unity3d.com/Manual/SL-Properties.html
        [MainColor] _BaseColor("Color", Color) = (0.5,0.5,0.5,1)
        [MainTexture][noscaleOffset] _BaseMap("Albedo", 2D) = "white" {}
        _CoverMap ("Cover Texture", 2D) = "white" {}
        _MaskColor ("Mask Color", Color) = (1,0,0,1)

        [Header(Stencil)]
        [BitMask255]_Stencil("Stencil ID", Range(0,255)) = 0

        // https://docs.unity3d.com/ScriptReference/Rendering.CompareFunction.html
        // Disable = 0
        // Never = 1
        // Less = 2
        // Equal = 3
        // LessEqual = 4
        // Greater = 5
        // NotEqual = 6
        // GreaterEqual = 7
        // Always = 8 (default)
        [Enum(UnityEngine.Rendering.CompareFunction)]_StencilComp("Stencil Comparison", Float) = 8

        [BitMask255] _StencilWriteMask ("Stencil Write Mask", Range(0,255)) = 255
        [BitMask255] _StencilReadMask ("Stencil Read Mask", Range(0,255)) = 255
        [Enum(UnityEngine.Rendering.StencilOp)]_StencilPass ("Stencil Pass", Float) = 0
        [Enum(UnityEngine.Rendering.StencilOp)]_StencilFail ("Stencil Fail", Float) = 0
        [Enum(UnityEngine.Rendering.StencilOp)]_StencilZFail ("Stencil ZFail", Float) = 0
        [Space]
        [Enum(UnityEngine.Rendering.CompareFunction)] _ZTest("ZTest", Float) = 0
        [Space]
        [Toggle] _Cull ("Cull", Float) = 1
        [MaterialToggle] _ZWrite ("Z-Write", Float) = 1

        [Header(Alpha Blend)]
        [Enum(UnityEngine.Rendering.BlendMode)] _BlendSrc("Blend Src", Float) = 0
        [Enum(UnityEngine.Rendering.BlendMode)] _BlendDst("Blend Dst", Float) = 0
    }

    SubShader
    {
        // https://docs.unity3d.com/Manual/SL-SubShaderTags.html
        Tags {
            "RenderType" = "Transparent"
            "Queue" = "Geometry" // Queue : { Background, Geometry, AlphaTest, Transparent, Overlay }
            "RenderPipeline" = "UniversalRenderPipeline"
            //"DisableBatching" = "True"
        }
        // https://docs.unity3d.com/Manual/SL-ShaderLOD.html
        LOD 300
        
        
        Pass
        {
            Name "URP Lit"
			Tags {
				"LightMode" = "UniversalForward"
			}
            
            HLSLPROGRAM
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

            #pragma vertex      LitPassVertex
            #pragma fragment    LitPassFragment
            #pragma target      3.0

            TEXTURE2D(_BaseMap);          SAMPLER(sampler_BaseMap);
            TEXTURE2D(_CoverMap);          SAMPLER(sampler_CoverMap);
            UNITY_INSTANCING_BUFFER_START(UnityPerMaterial)
                UNITY_DEFINE_INSTANCED_PROP(real4, _BaseMap_ST);
                UNITY_DEFINE_INSTANCED_PROP(real4, _CoverMap_ST);
                UNITY_DEFINE_INSTANCED_PROP(real4, _BaseColor);

            UNITY_INSTANCING_BUFFER_END(UnityPerMaterial)
            

            struct Attributes
            {
                float4 positionOS   : POSITION;
                float2 uv           : TEXCOORD0;
            };

            struct Varyings
            {
                float4 positionCS   : SV_POSITION;
                float3 positionWS   : TEXCOORD0;
                float2 uv           : TEXCOORD1;
            };

            Varyings LitPassVertex(Attributes i)
            {
                Varyings o;
                VertexPositionInputs vertexInput = GetVertexPositionInputs(i.positionOS.xyz);

                o.positionCS   = vertexInput.positionCS;
                o.positionWS   = vertexInput.positionWS;
                o.uv           = TRANSFORM_TEX(i.uv, _BaseMap);
                return o;
            }

            real4 LitPassFragment(Varyings i) : SV_Target
            {
                real4 albedo                = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, i.uv * _BaseMap_ST.xy + _BaseMap_ST.zw);
                albedo.rgb                  *= _BaseColor.rgb;

                return albedo;
            }
            ENDHLSL

        }


        Pass
        {
            Name "Stencil"
			Tags {
				"LightMode" = "SRPDefaultUnlit"
			}
            
            Stencil
			{
                // Stencil channel 0 ~ 255
				Ref         [_Stencil]

                // Test condition
                // https://docs.unity3d.com/ScriptReference/Rendering.CompareFunction.html
                // 
                // Disable      = 0
                // Never        = 1
                // Less         = 2
                // Equal        = 3
                // LessEqual    = 4
                // Greater      = 5
                // NotEqual     = 6
                // GreaterEqual = 7
                // Always       = 8 (default)
				Comp        [_StencilComp]
                
                // Test condition success action
                Pass        [_StencilPass] // default:Keep
                
                // Test condition fail action
                Fail        [_StencilFail]

                // Z-test fail action
                ZFail       [_StencilZFail]

				ReadMask    [_StencilReadMask]
                WriteMask   [_StencilWriteMask]
			}
            Cull    [_Cull]
            ZTest   [_ZTest]
            ZWrite  [_ZWrite]
            ColorMask 0

            // https://docs.unity3d.com/Manual/SL-Blend.html
            /// Blend SrcAlpha OneMinusSrcAlpha // Traditional transparency
            /// Blend One OneMinusSrcAlpha // Premultiplied transparency
            /// Blend One One // Additive
            /// Blend OneMinusDstColor One // Soft additive
            /// Blend DstColor Zero // Multiplicative
            /// Blend DstColor SrcColor // 2x multiplicative
            /// Remark : {0}% from this object, {1}% for color always render
            Blend   [_BlendSrc][_BlendDst]

            Color   [_MaskColor]

        }
    }
}