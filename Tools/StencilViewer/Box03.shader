Shader "Kit/Universal Render Pipeline/Tools/Stencil Box 03"
{
    Properties
    {
        [MainColor] _BaseColor("Color", Color) = (0.5,0.5,0.5,1)
        [MainTexture][noscaleOffset] _BaseMap("Albedo", 2D) = "white" {}
        _MaskColor ("Mask Color", Color) = (1,0,0,1)

        [BitMask255]_Stencil("Stencil ID", Range(0,255)) = 0

        // https://docs.unity3d.com/ScriptReference/Rendering.CompareFunction.html
        // Disable = 0
        // Never = 1
        // Less = 2
        // Equal = 3
        // LessEqual = 4
        // Greater = 5, apply when this greater then other
        // NotEqual = 6
        // GreaterEqual = 7
        // Always = 8 (default)
        [Enum(UnityEngine.Rendering.CompareFunction)]_StencilComp("Stencil Comparison", Float) = 3

        [BitMask255]_StencilWriteMask ("Stencil Write Mask", Range(0,255)) = 255
        [BitMask255]_StencilReadMask ("Stencil Read Mask", Range(0,255)) = 255
        [Enum(UnityEngine.Rendering.StencilOp)]_StencilPass ("Stencil Pass", Float) = 0
        [Enum(UnityEngine.Rendering.StencilOp)]_StencilFail ("Stencil Fail", Float) = 0
        [Enum(UnityEngine.Rendering.StencilOp)]_StencilZFail ("Stencil ZFail", Float) = 0
    }

    SubShader
    {
        // https://docs.unity3d.com/Manual/SL-SubShaderTags.html
        Tags {
            "RenderType" = "Opaque"
            "Queue" = "Geometry" // Queue : { Background, Geometry, AlphaTest, Transparent, Overlay }
            "RenderPipeline" = "UniversalRenderPipeline"
            //"DisableBatching" = "True"
        }
        LOD 100
        
        Pass
        {
            Name "URP Lit"
			Tags {
				"LightMode" = "UniversalForward"
			}
            
            HLSLPROGRAM
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

            #pragma vertex      LitPassVertex
            #pragma fragment    LitPassFragment
            #pragma target      3.0

            TEXTURE2D(_BaseMap);          SAMPLER(sampler_BaseMap);
            UNITY_INSTANCING_BUFFER_START(UnityPerMaterial)
                UNITY_DEFINE_INSTANCED_PROP(real4, _BaseMap_ST);
                UNITY_DEFINE_INSTANCED_PROP(real4, _BaseColor);
            UNITY_INSTANCING_BUFFER_END(UnityPerMaterial)
            

            struct Attributes
            {
                float4 positionOS   : POSITION;
                float2 uv           : TEXCOORD0;
            };

            struct Varyings
            {
                float4 positionCS   : SV_POSITION;
                float3 positionWS   : TEXCOORD0;
                float2 uv           : TEXCOORD1;
            };

            Varyings LitPassVertex(Attributes i)
            {
                Varyings o;
                VertexPositionInputs vertexInput = GetVertexPositionInputs(i.positionOS.xyz);

                o.positionCS   = vertexInput.positionCS;
                o.positionWS   = vertexInput.positionWS;
                o.uv           = TRANSFORM_TEX(i.uv, _BaseMap);
                return o;
            }

            real4 LitPassFragment(Varyings i) : SV_Target
            {
                real4 albedo                = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, i.uv * _BaseMap_ST.xy + _BaseMap_ST.zw);
                albedo.rgb                  *= _BaseColor.rgb;

                return albedo;
            }
            ENDHLSL

        }

        Pass
        {
            Name "Stencil"
			Tags {
				"LightMode" = "SRPDefaultUnlit"
			}
            
            Stencil
			{
                // Stencil channel 0 ~ 255
				Ref         [_Stencil]

                // Test condition
                // https://docs.unity3d.com/ScriptReference/Rendering.CompareFunction.html
                // Disable      = 0
                // Never        = 1
                // Less         = 2
                // Equal        = 3
                // LessEqual    = 4
                // Greater      = 5
                // NotEqual     = 6
                // GreaterEqual = 7
                // Always       = 8 (default)
				Comp        [_StencilComp]

                // Test condition success action
                Pass        [_StencilPass]

                // Test condition fail action
                Fail        [_StencilFail]

                // Z-test fail.
                ZFail       [_StencilZFail]

				ReadMask    [_StencilReadMask]
                WriteMask   [_StencilWriteMask]
			}
            Cull    Off
            ZTest   LEqual
            ZWrite  Off

            // {0}% from this object, {1}% for color always render
            Blend   One Zero
            Color   [_MaskColor]
        }
    }
}