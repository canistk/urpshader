Shader "Kit/Universal Render Pipeline/Tools/Stencil Viewer"
{
    Properties
    {
        _BgColor("Background", color) = (0.0,0.0,0.0,0.5)
        _Color("Debug Color", color) = (0.0,1.0,0.0,1.0)

        //====================================== below = usually can ignore in normal use case =====================================================================
        [Header(Stencil)]
        [BitMask255]_Stencil("Stencil ID", Range(0,255)) = 0

        // https://docs.unity3d.com/ScriptReference/Rendering.CompareFunction.html
        // Disable = 0
        // Never = 1
        // Less = 2
        // Equal = 3
        // LessEqual = 4
        // Greater = 5, apply when this greater then other
        // NotEqual = 6
        // GreaterEqual = 7
        // Always = 8 (default)
        [Enum(UnityEngine.Rendering.CompareFunction)]_StencilComp("Stencil Comparison", Float) = 3

        [BitMask255]_StencilWriteMask ("Stencil Write Mask", Range(0,255)) = 255
        [BitMask255]_StencilReadMask ("Stencil Read Mask", Range(0,255)) = 255
        [Enum(UnityEngine.Rendering.StencilOp)]_StencilPass ("Stencil Pass", Float) = 0
        [Enum(UnityEngine.Rendering.StencilOp)]_StencilFail ("Stencil Fail", Float) = 0
        [Enum(UnityEngine.Rendering.StencilOp)]_StencilZFail ("Stencil ZFail", Float) = 0
    }

    SubShader
    {
        // https://docs.unity3d.com/Manual/SL-SubShaderTags.html
        Tags {
            "RenderType" = "Transparent"
            "Queue" = "Overlay" // Queue : { Background, Geometry, AlphaTest, Transparent, Overlay }
            "RenderPipeline" = "UniversalPipeline"
            "DisableBatching" = "True"
        }
        LOD 100
        

        Pass
        {
            Name "ForwardLit"
			Tags {
				"LightMode" = "UniversalForward"
			}
			Cull    Off
            ZTest   LEqual
            ZWrite  Off
            Blend   SrcAlpha OneMinusSrcAlpha
            Color   [_BgColor]
        }
        Pass
        {
            Name "DebugStencil"
			Tags {
				"LightMode" = "SRPDefaultUnlit"
			}
            Stencil
			{
                // Stencil channel 0 ~ 255
				Ref         [_Stencil]

                // Test condition
                // https://docs.unity3d.com/ScriptReference/Rendering.CompareFunction.html
                // Disable      = 0
                // Never        = 1
                // Less         = 2
                // Equal        = 3
                // LessEqual    = 4
                // Greater      = 5
                // NotEqual     = 6
                // GreaterEqual = 7
                // Always       = 8 (default)
				Comp        [_StencilComp]
                
                // Test condition success action
                Pass        [_StencilPass] // default:Keep
                
                // Test condition fail action
                Fail        [_StencilFail]

                // Z-test fail action
                ZFail       [_StencilZFail]

				ReadMask    [_StencilReadMask]
                WriteMask   [_StencilWriteMask]
			}
            Cull    Off
            ZTest   Always
            ZWrite  Off
            Blend   One Zero
            Color   [_Color]
        }
    }
}