//Shader controls how much light is passed through object when the light source is behind the surface currently being rendered. 
//This can be useful for materials such as cloth or vegetation.

Shader "Kit/Universal Render Pipeline/Transmission" 
{
	Properties 
	{
		_MainTex ("Diffuse map", 2D) = "white" {}
		_Range ("Range", Range(0, 3)) = 1
	}
	SubShader 
	{
		Cull Off
		Tags {
			"RenderType"="Opaque"
		}
		Pass 
		{
			// https://docs.unity3d.com/Manual/SL-SubShaderTags.html
			Tags {
				"RenderType" = "Opaque"
				"Queue" = "Geometry" // Queue : { Background, Geometry, AlphaTest, Transparent, Overlay }
				"RenderPipeline" = "UniversalPipeline"
				// "DisableBatching" = "True"
			}
			LOD 100
			HLSLPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			CBUFFER_START(UnityPerMaterial)
			sampler2D _MainTex;
			float4 _LightColor0, _MainTex_ST;
			float _Range;
			CBUFFER_END
			
			struct Attributes
            {
                float4 positionOS   : POSITION;
                float3 normalOS     : NORMAL;
				float4 tangentOS    : TANGENT;
                float2 uv           : TEXCOORD0;
            };

			struct Varyings
			{
				float4 positionCS               : SV_POSITION;
				float2 uv						: TEXCOORD0;
				half3  normalWS                 : NORMAL;
			};
			
			Varyings vert(Attributes input) 
			{
				Varyings output;

                // VertexPositionInputs contains position in multiple spaces (world, view, homogeneous clip space)
                // Our compiler will strip all unused references (say you don't use view space).
                // Therefore there is more flexibility at no additional cost with this struct.
                VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);

                // Similar to VertexPositionInputs, VertexNormalInputs will contain normal, tangent and bitangent
                // in world space. If not used it will be stripped.
                VertexNormalInputs vertexNormalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);

				output.positionCS = vertexInput.positionCS;
				output.normalWS = vertexNormalInput.normalWS;
				output.uv = TRANSFORM_TEX(input.uv, _MainTex);;
				return output;
			}
			
			float4 frag(Varyings IN) : SV_Target 
			{
				float3 AmbientLight = UNITY_LIGHTMODEL_AMBIENT.rgb; 			
				float3 NormalDirection = IN.normalWS;
				float3 LightDirection = normalize(_MainLightPosition.xyz);
				float3 LightColor = _MainLightColor.rgb;

				float NdotL = dot( NormalDirection, LightDirection );
				float3 ForwardLight = max(0.0, NdotL);
				float3 BackLight = max(0.0, -NdotL ) * float3(_Range,_Range,_Range);
				float3 DiffuseColor = (ForwardLight+BackLight) * LightColor + AmbientLight;
				float3 DiffuseMap = tex2D(_MainTex,IN.uv*_MainTex_ST.xy+_MainTex_ST.zw).rgb;
				return float4(DiffuseColor*DiffuseMap,1.0);
			}
			ENDHLSL
		}
	}
}