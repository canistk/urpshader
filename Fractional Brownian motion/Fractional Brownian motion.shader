Shader "Kit/Universal Render Pipeline/Fractional Brownian motion Test"
{
    Properties
    {
        [MainColor] _BaseColor("Color", Color) = (0.5,0.5,0.5,1)
        [MainTexture] _BaseMap("Albedo", 2D) = "white" {}

        _Speed("Motion Speed", Range(-10.0, 10.0)) = 1.0
        _Hurst("Hurst exponent", Range(0.0, 2.0)) = 0.5
        _Lacunarity("lacunarity", Range(-6.0, 6.0)) = 2
        [IntRange]_NumOfOctave("Num of Octave", Range(1, 10)) = 5

        [Toggle(_EnableDeform)] _EnableDeform ("Enable deform", Float) = 0
       
    }

    SubShader
    {
        Tags{
            "RenderType" = "Opaque"
            "Queue" = "Geometry" // Queue : { Background, Geometry, AlphaTest, Transparent, Overlay }
            "RenderPipeline" = "UniversalRenderPipeline"
            // "IgnoreProjector" = "True"
        }
        // https://docs.unity3d.com/Manual/SL-ShaderLOD.html
        LOD 300

        // ------------------------------------------------------------------
        // Forward pass. Shades GI, emission, fog and all lights in a single pass.
        // Compared to Builtin pipeline forward renderer, LWRP forward renderer will
        // render a scene with multiple lights with less drawcalls and less overdraw.
        Pass
        {
            // "Lightmode" tag must be "UniversalForward" or not be defined in order for
            // to render objects.
            Name "CustomCanisLit"
            Tags {
                "LightMode" = "UniversalForward"
            }

            HLSLPROGRAM
            #pragma target 4.0
            #pragma vertex LitPassVertex
            #pragma fragment LitPassFragment
            #pragma shader_feature_local _EnableDeform
            // #include "KitShader.hlsl"
            // #include_with_pragmas "Kit_DetailTexture.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            
            struct Attributes
            {
                float4 positionOS   : POSITION;
                float3 normalOS     : NORMAL;
                float4 tangentOS    : TANGENT;
                float2 uv           : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct Varyings
            {
                half4 positionCS                : SV_POSITION;
                half3 positionWS                : TEXCOORD0; // xyz: positionWS, w: vertex fog factor
                half3 normalWS                  : TEXCOORD1;
                half3 tangentWS                 : TEXCOORD2;
                half3 bitangentWS               : TEXCOORD3;
                half2 uv                        : TEXCOORD4;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                UNITY_VERTEX_OUTPUT_STEREO
            };

            TEXTURE2D(_BaseMap);          SAMPLER(sampler_BaseMap);
            // In order to support VR & GPU Instancing
            UNITY_INSTANCING_BUFFER_START(UnityPerMaterial)
                UNITY_DEFINE_INSTANCED_PROP(half4, _BaseMap_ST);
                UNITY_DEFINE_INSTANCED_PROP(half4, _BaseColor);
                UNITY_DEFINE_INSTANCED_PROP(half, _Speed);
                UNITY_DEFINE_INSTANCED_PROP(half, _Hurst);
                UNITY_DEFINE_INSTANCED_PROP(half, _Lacunarity);
                UNITY_DEFINE_INSTANCED_PROP(half, _NumOfOctave);
                
            UNITY_INSTANCING_BUFFER_END(UnityPerMaterial)
            
            // generate pseudorandom value between 0.0 and 1.0
            real random (real2 uv)
            {
                return frac(sin(dot(uv,real2(12.9898,78.233)))*43758.5453123);
            }

            // Based on Morgan McGuire @morgan3d
            // https://www.shadertoy.com/view/4dS3Wd
            // bilinear interpolation
            // https://forum.unity.com/threads/scripting-textures.520057/
            // https://gist.github.com/sneha-belkhale/d944211b9af1e3575392d4e460676f30
            real noise (in real2 _st) {
                real2 i = floor(_st);
                real2 f = frac(_st);

                // Four corners in 2D of a tile
                real a = random(i);
                real b = random(i + real2(1.0, 0.0));
                real c = random(i + real2(0.0, 1.0));
                real d = random(i + real2(1.0, 1.0));
                
                /*
                // Too slow!
                real2 u = smoothstep(0.0, 1.0, f);
                real r = lerp( lerp(a,b,f.x), lerp(c,d,f.x), f.y);
                return r * r;

                // Same code, with the clamps in smoothstep and common subexpressions
	            // optimized away. 3 lerp > 1 lerp.
                */
	            real2 u = f * f * (3.0 - 2.0 * f);
	            return lerp(a, b, u.x) + (c - a) * u.y * (1.0 - u.x) + (d - b) * u.x * u.y;
            }

            // Fractional Brownian motion
            // https://iquilezles.org/articles/fbm/
            // https://gist.github.com/patriciogonzalezvivo/670c22f3966e662d2f83
            // https://thebookofshaders.com/13/
            real fbm( in real2 sample, in real lacunarity, in real Hurst, in int numOfOctave)
            {
                real gain = exp2(-Hurst);
                real frequency = 1.0;
                real amplitude = 1.0;
                real value = 0.0;
                for( int i=0; i<numOfOctave; i++ )
                {
                    value += amplitude * noise(frequency * sample);
                    frequency *= lacunarity;
                    amplitude *= gain;
                }
                return value;
            }


            Varyings LitPassVertex(Attributes IN)
            {
                Varyings OUT = (Varyings)0;
                
                // https://docs.unity3d.com/Manual/SinglePassInstancing.html // Support in 2020.3
                UNITY_SETUP_INSTANCE_ID(IN);
                //UNITY_INITIALIZE_OUTPUT(Varyings, OUT); // Support in 2020.3
                // https://docs.unity3d.com/Manual/SinglePassStereoRendering.html
                // https://docs.unity3d.com/Manual/Android-SinglePassStereoRendering.html
                UNITY_TRANSFER_INSTANCE_ID(IN, OUT); // necessary only if you want to access instanced properties in the fragment Shader.
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT); // VR support - Single-Pass Stereo Rendering for Android

                #if _EnableDeform 
                // Fractional Brownian motion
                half hurst                  = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _Hurst);
                half lacunarity             = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _Lacunarity);
                int num                     = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _NumOfOctave);
                half speed                  = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _Speed);

                half2 src                   = half2(_SinTime.x * speed, _CosTime.x * speed);
                half2 p                     = half2(IN.positionOS.z, fbm(src, lacunarity, hurst, num));
                half2 pattern2              = half2 (   fbm(p, lacunarity, hurst, num),
                                                        fbm(p + half2(5.2, 1.3), lacunarity, hurst, num));
                //half2 pattern3              = half2 (   fbm(p + 4.0 * pattern2 + half2(1.7,9.2), lacunarity, hurst, num),
                //                                        fbm(p + 4.0 * pattern2 + half2(8.3,2.8), lacunarity, hurst, num));
                //half pattern4               = fbm(p + 4.0 * pattern3, lacunarity, hurst, num);


                real3 posOS = IN.positionOS.xyz + (IN.normalOS * pattern2.x);
                #else
                real3 posOS = IN.positionOS.xyz;
                #endif

                VertexPositionInputs vertexInput = GetVertexPositionInputs(posOS);
                VertexNormalInputs normalInput = GetVertexNormalInputs(IN.normalOS, IN.tangentOS);

                // TRANSFORM_TEX is the same as the old shader library.
                OUT.uv = IN.uv; // TRANSFORM_TEX(IN.uv, _BaseMap);

                // We just use the homogeneous clip position from the vertex input
                OUT.positionCS = vertexInput.positionCS;
                OUT.positionWS = vertexInput.positionWS;

                // Here comes the flexibility of the input structs.
                // In the variants that don't have normal map defined
                // tangentWS and bitangentWS will not be referenced and
                // GetnormalInputs is only converting normal
                // from object to world space
                OUT.normalWS = normalInput.normalWS;
                OUT.tangentWS = normalInput.tangentWS;
                OUT.bitangentWS = normalInput.bitangentWS;

                return OUT;
            }

            half4 LitPassFragment(Varyings IN) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID(IN); // necessary only if any instanced properties are going to be accessed in the fragment Shader.
                UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(IN); // VR support - Single-Pass Stereo Rendering for Android

                half4 i_BaseColor           = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _BaseColor);
                half4 i_BaseMap_ST          = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _BaseMap_ST);
                
                // Fractional Brownian motion
                half hurst                  = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _Hurst);
                half lacunarity             = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _Lacunarity);
                int num                     = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _NumOfOctave);
                half speed                  = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _Speed);
                

                half2 uv                    = IN.uv;
                half2 pattern2              = half2 (   fbm(uv, lacunarity, hurst, num),
                                                        fbm(uv + half2(5.2, 1.3), lacunarity, hurst, num));
                half2 pattern3              = half2 (   fbm(uv + 4.0 * pattern2 + half2(1.7,9.2), lacunarity, hurst, num),
                                                        fbm(uv + 4.0 * pattern2 + half2(8.3,2.8), lacunarity, hurst, num));
                half2 pattern4              = half2 (   fbm(uv + 4.0 * pattern3, lacunarity, hurst, num),
                                                        fbm(uv + 4.0 * pattern2 + half2(2.4,1.7), lacunarity, hurst, num));
                half2 finalUV               = pattern4 + _Time.x * speed;

                // Sampling UV based on pattern.
                half4 albedo                = SAMPLE_TEXTURE2D_LOD(_BaseMap, sampler_BaseMap, finalUV * i_BaseMap_ST.xy + i_BaseMap_ST.zw, 0);
                albedo.rgb                  *= i_BaseColor.rgb;

                
                return albedo;
            }
            ENDHLSL
        }

        // Used for rendering shadowmaps
        UsePass "Universal Render Pipeline/Lit/ShadowCaster"

        // Used for depth prepass
        // If shadows cascade are enabled we need to perform a depth prepass. 
        // We also need to use a depth prepass in some cases camera require depth texture
        // (e.g, MSAA is enabled and we can't resolve with Texture2DMS
        UsePass "Universal Render Pipeline/Lit/DepthOnly"

        UsePass "Universal Render Pipeline/Lit/DepthNormals"

        // Used for Baking GI. This pass is stripped from build.
        UsePass "Universal Render Pipeline/Lit/Meta"
    }

    // Uses a custom shader GUI to display settings. Re-use the same from Lit shader as they have the
    // same properties.
    // CustomEditor "UnityEditor.Rendering.Universal.ShaderGUI.LitShader"
}