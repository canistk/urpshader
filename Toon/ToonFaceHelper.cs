using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(Renderer))]
public class ToonFaceHelper : MonoBehaviour
{
    [SerializeField] Transform m_Orientation;
    private Renderer m_Renderer;
    public Renderer render
    {
        get
        {
            if (m_Renderer == null)
                m_Renderer = GetComponent<Renderer>();
            return m_Renderer;
        }
    }

    private Shader m_ToonFace = null;
    private Shader toonFace
    {
        get
        {
            if (m_ToonFace == null)
                m_ToonFace = Shader.Find("Kit/Universal Render Pipeline/Toon Face");
            return m_ToonFace;
        }
    }

    private int _FaceFrontDirection = Shader.PropertyToID("_FaceFrontDirection");
    private int _FaceRightDirection = Shader.PropertyToID("_FaceRightDirection");
    private int _FaceUpDirection = Shader.PropertyToID("_FaceUpDirection");
    private MaterialPropertyBlock m_MB = null;

    private void OnEnable()
    {
    }

    private void OnDisable()
    {
        for (int i = 0; i < render.sharedMaterials.Length; ++i)
        {
            var mat = render.sharedMaterials[i];
            if (!mat.shader.Equals(toonFace))
                continue;
            render.SetPropertyBlock(null, i);
        }
        m_MB = null;
    }

    private void LateUpdate()
    {
        var face = m_Orientation ? m_Orientation : transform;
        if (m_MB == null)
            m_MB = new MaterialPropertyBlock();
        m_MB.SetVector(_FaceFrontDirection, (Vector4)face.forward);
        m_MB.SetVector(_FaceRightDirection, (Vector4)face.right);
        m_MB.SetVector(_FaceUpDirection, (Vector4)face.up);

        for (int i = 0; i < render.sharedMaterials.Length; ++i)
        {
            var mat = render.sharedMaterials[i];
            if (!mat.shader.Equals(toonFace))
                continue;
            render.SetPropertyBlock(m_MB, i);
        }
    }
}
