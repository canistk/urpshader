Shader "Kit/Universal Render Pipeline/Toon Face"
{
    Properties
    {
        [MainColor]_Color("Base Color", Color) = (1,1,1,1)
        [MainTexture]_MainTex ("Main Texture", 2D) = "white" {}
        [Space(10)]
        
        [Header(Skin Ramp)]
        [noscaleOffset]_SkinRamp       ("Skin Ramp", 2D) = "white" {}
        _SkinRampHigh   ("high",    Range(0.0, 1.0)) = 1.0
        _SkinRampLow    ("low",     Range(0.0, 1.0)) = 0.0
        _SkinRampWeight ("weight",   Range(0.0, 1.0)) = 1.0
        [Header(Additional light ramp)]
        _SkinRampAddHigh("high",        Range(0.0, 1.0)) = 1.0
        _SkinRampAddLow ("low",         Range(0.0, 1.0)) = 0.0
        _SkinRampAddWeight ("weight",   Range(0.0, 1.0)) = 1.0

        [Header(MatCap)]
        [noscaleOffset]_MatCap         ("MatCap (RGB)",        2D) = "white" {}
        [noscaleOffset]_MatCapMask     ("MatCap (Mask)",       2D) = "white" {}
        _MatCapWeight   ("MatCap (Weight)", Range(0.0, 1.0)) = 1.0

        [Header(Shadow Map direction bias)]
        [NoScaleOffset]_ShadowMapTop   ("ShadowMap (Top)",     2D) = "white" {}
        [NoScaleOffset]_ShadowMapMiddle("ShadowMap (Middle)",  2D) = "white" {}
        [NoScaleOffset]_ShadowMapBottom("ShadowMap (Bottom)",  2D) = "white" {}
        _ShadowMapTopDown("ShadowMap top down anchor",      Range(0.3, 0.8)) = 0.71
        _ShadowMapBottomUp("ShadowMap bottom up anchor",    Range(0.3, 0.8)) = 0.71
        [HDR]_ShadowMapColor("Shadow Color", Color)= (0, 0, 0, 1)
        _ShadowMapWeight("ShadowMap (Weight)", Range(0.0, 1.0)) = 0.0
        _FaceShadowMapPow("ShadowMap power", Range(0.001, 0.5)) = 0.001
        _FaceShadowSmoothness("ShadowMap smoothness", Range(0.0, 0.5)) = 0
        _FaceShadowMapMainLightBias("ShadowMap Main light(Bias)", Range(0.0, 1.0)) = 1.0
        _FaceShadowMapSubLightBias("ShadowMap Additional light(Bias)", Range(0.0, 1.0)) = 0.0

        [Space(10)]
        _FaceFrontDirection ("Face Front Direction", Vector) = (0, 0, 1, 0)
        _FaceRightDirection ("Face Right Direction", Vector) = (1, 0, 0, 0)
        _FaceUpDirection("Face Up Direction", Vector) = (0, 1, 0, 0)
    }

    SubShader
    {
        // https://docs.unity3d.com/Manual/SL-SubShaderTags.html
        Tags {
            "RenderType" = "Opaque"
            "Queue" = "Geometry" // Queue : { Background, Geometry, AlphaTest, Transparent, Overlay }
            "RenderPipeline" = "UniversalPipeline"
            "DisableBatching" = "True"
        }
        LOD 100
        Cull Back
        ZWrite On
		
        Pass
        {
            HLSLPROGRAM
            #pragma vertex LitPassVertex
            #pragma fragment LitPassFragment
            //#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
            //#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
            #pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
            // https://github.com/Unity-Technologies/Graphics/blob/master/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            // Include this if you are doing a lit shader. This includes lighting shader variables,
            // lighting and shadow functions
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

            #include "../ShaderLibrary/KitShader.hlsl"
            
            #define MY_TEXTURE(name, uv) SAMPLE_TEXTURE2D(name, sampler##name, uv.xy * name##_ST.xy + name##_ST.zw)

            //TEXTURE2D(_BaseMap);      SAMPLER(sampler_BaseMap);
            TEXTURE2D(_MainTex);        SAMPLER(sampler_MainTex);
            TEXTURE2D(_SkinRamp);       SAMPLER(sampler_SkinRamp);
            TEXTURE2D(_MatCap);         SAMPLER(sampler_MatCap);
            TEXTURE2D(_MatCapMask);     SAMPLER(sampler_MatCapMask);
            TEXTURE2D(_ShadowMapTop);   SAMPLER(sampler_ShadowMapTop);
            TEXTURE2D(_ShadowMapMiddle);   SAMPLER(sampler_ShadowMapMiddle);
            TEXTURE2D(_ShadowMapBottom);   SAMPLER(sampler_ShadowMapBottom);
            
            
            UNITY_INSTANCING_BUFFER_START(UnityPerMaterial)
                UNITY_DEFINE_INSTANCED_PROP(real4,  _MainTex_ST);
                UNITY_DEFINE_INSTANCED_PROP(real4,  _FaceFrontDirection);
                UNITY_DEFINE_INSTANCED_PROP(real4,  _FaceRightDirection);
                UNITY_DEFINE_INSTANCED_PROP(real4,  _FaceUpDirection);
                UNITY_DEFINE_INSTANCED_PROP(real4,  _Color);
                UNITY_DEFINE_INSTANCED_PROP(real4,  _ShadowMapColor);
                UNITY_DEFINE_INSTANCED_PROP(real,   _SkinRampHigh);
                UNITY_DEFINE_INSTANCED_PROP(real,   _SkinRampLow);
                UNITY_DEFINE_INSTANCED_PROP(real,   _SkinRampAddHigh);
                UNITY_DEFINE_INSTANCED_PROP(real,   _SkinRampAddLow);

                UNITY_DEFINE_INSTANCED_PROP(real,   _SkinRampWeight);
                UNITY_DEFINE_INSTANCED_PROP(real,   _SkinRampAddWeight);
                UNITY_DEFINE_INSTANCED_PROP(real,   _MatCapWeight);
                UNITY_DEFINE_INSTANCED_PROP(real,   _ShadowMapWeight);

                UNITY_DEFINE_INSTANCED_PROP(real,   _FaceShadowMapPow);
                UNITY_DEFINE_INSTANCED_PROP(real,   _FaceShadowSmoothness);
                UNITY_DEFINE_INSTANCED_PROP(real,   _FaceShadowMapMainLightBias);
                UNITY_DEFINE_INSTANCED_PROP(real,   _FaceShadowMapSubLightBias);

                UNITY_DEFINE_INSTANCED_PROP(real,   _ShadowMapTopDown);
                UNITY_DEFINE_INSTANCED_PROP(real,   _ShadowMapBottomUp);
            UNITY_INSTANCING_BUFFER_END(UnityPerMaterial)
            
            struct Attributes
            {
                // The positionOS variable contains the vertex positions in object
                // space.
                real4 positionOS    : POSITION;
                real3 normalOS      : NORMAL;
                real2 uv            : TEXCOORD0;
                real4 color         : COLOR;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct Varyings
            {
                // The positions in this struct must have the SV_POSITION semantic.
                real4 positionCS    : SV_POSITION;
                real4 color         : COLOR;
                real3 normalWS      : NORMAL;
                real4 uv_matcap     : TEXCOORD0; // uv = xy, matcap = zw
                real3 positionWS    : TEXCOORD1;
                real3 vertexLight   : TEXCOORD2;
#ifdef _MAIN_LIGHT_SHADOWS
                real4 shadowCoord   : TEXCOORD3; // compute shadow coord per-vertex for the main light
#endif
                
                UNITY_VERTEX_INPUT_INSTANCE_ID
                UNITY_VERTEX_OUTPUT_STEREO
            };

            InputData GetInputData(Varyings IN)
            {
                InputData inputData;
                inputData.positionWS        = IN.positionWS;
                inputData.normalWS          = IN.normalWS;
                inputData.viewDirectionWS   = SafeNormalize(GetCameraPositionWS() - IN.positionWS);
                inputData.fogCoord          = ComputeFogFactor(IN.positionCS.z);
                inputData.vertexLighting    = IN.vertexLight;
#if _MAIN_LIGHT_SHADOWS
                inputData.shadowCoord       = IN.shadowCoord;
#else
                inputData.shadowCoord       = 0;
#endif
                inputData.normalizedScreenSpaceUV = GetNormalizedScreenSpaceUV(IN.positionCS);
                // To ensure backward compatibility we have to avoid using shadowMask input, as it is not present in older shaders
#if defined(SHADOWS_SHADOWMASK) && defined(LIGHTMAP_ON)
                real4 shadowMask = inputData.shadowMask;
#elif !defined (LIGHTMAP_ON)
                real4 shadowMask = unity_ProbesOcclusion;
#else
                real4 shadowMask = real4(1, 1, 1, 1);
#endif
                inputData.shadowMask        = shadowMask;
                return inputData;
            }
            
            real2 CalcMapcapUV(real3 normal)
            {
                real3 worldNormal = normalize(
                    unity_WorldToObject[0].xyz * normal.x +
                    unity_WorldToObject[1].xyz * normal.y +
                    unity_WorldToObject[2].xyz * normal.z);
                worldNormal = mul((real3x3)UNITY_MATRIX_V, worldNormal);
                return worldNormal.xy * 0.5 + 0.5; // -1~1 remap 0~1
            }

            Varyings LitPassVertex(Attributes IN)
            {
                // https://docs.unity3d.com/Manual/SinglePassInstancing.html // Support in 2020.3
                UNITY_SETUP_INSTANCE_ID(IN);
                //UNITY_INITIALIZE_OUTPUT(Varyings, OUT); // Support in 2020.3
                // https://docs.unity3d.com/Manual/SinglePassStereoRendering.html
                // https://docs.unity3d.com/Manual/Android-SinglePassStereoRendering.html
                UNITY_TRANSFER_INSTANCE_ID(IN, OUT); // necessary only if you want to access instanced properties in the fragment Shader.
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT); // VR support - Single-Pass Stereo Rendering for Android

                Varyings OUT;
                VertexPositionInputs vertexInput = GetVertexPositionInputs(IN.positionOS.xyz);
                
                OUT.positionCS = vertexInput.positionCS;
                OUT.positionWS = vertexInput.positionWS;
                OUT.normalWS = TransformObjectToWorldNormal(IN.normalOS);
                OUT.color = IN.color;
                OUT.uv_matcap   = real4(IN.uv, CalcMapcapUV(OUT.normalWS));
                OUT.vertexLight = VertexLighting(OUT.positionWS, OUT.normalWS);
#ifdef _MAIN_LIGHT_SHADOWS
                // shadow coord for the main light is computed in vertex.
                // If cascades are enabled, LWRP will resolve shadows in screen space
                // and this coord will be the uv coord of the screen space shadow texture.
                // Otherwise LWRP will resolve shadows in light space (no depth pre-pass and shadow collect pass)
                // In this case shadowCoord will be the position in light space.
                OUT.shadowCoord = GetShadowCoord(vertexInput);
#endif
                return OUT;
            }

            struct BiasData
            {
                real NdotL;
                real NdotL01;
                real3 color;
                real attenuation;
            };

            BiasData BiasLight(real3 normalWS, Light light, real high, real low, real weight)
            {
                BiasData ramp;
                real NdotL01        = dot(normalize(normalWS), light.direction) * 0.5 + 0.5;
                real adjNdotL01     = smoothstep(low, high, saturate(invLerp(low,high, NdotL01)));
                ramp.NdotL01        = weight * adjNdotL01;
                ramp.NdotL          = NdotL01 * 2.0 - 1.0;
                ramp.color          = light.color.rgb;
                ramp.attenuation    = light.distanceAttenuation;// * light.shadowAttenuation;
                return ramp;
            }
            
            
            real RimLighting(real4 rimColor, real3 lightDir, real3 normal, real3 viewDir, real rimPower, real rimLightAlign, real rimSmoothmess)
            {
                rimPower        = 1.0 - rimPower;
                real NdotL      = saturate(dot(lightDir, normal));
                real rim        = saturate((1.0 - dot(viewDir, normal)) * lerp(1, NdotL, saturate(rimLightAlign)) * lerp(1, 1 - NdotL, saturate(-rimLightAlign)));
                real delta      = fwidth(rim);
                return smoothstep(rimPower - delta, rimPower + delta + rimSmoothmess, rim) * rimColor.rgb * rimColor.a;
            }

            /*
            logic rewrite by Canis

            logic rewrite by
            link : https://assetstore.unity.com/packages/vfx/shaders/otoon-urp-toon-shading-216102
            // OToon > Lighting_Extend.hlsl

            The face shadow map logic is taken from github repo by user Ash Yukiha.
            link : https://github.com/ashyukiha/GenshinCharacterShaderZhihuVer
            */
            real FaceShadowMap(real2 uv, real3 lightDir)
            {
                real    smoothness  = _FaceShadowSmoothness;
                real    power       = _FaceShadowMapPow;
                real3   front       = normalize(_FaceFrontDirection.xyz);
                real3   right       = normalize(_FaceRightDirection.xyz);
                real3   up          = normalize(_FaceUpDirection.xyz);

                // real3   lightDir    = light.direction.xyz;
                real    FrontL      = dot((front.xz), (lightDir.xz));
                real    RightL      = dot(normalize(right.xz), normalize(lightDir.xz));
                real    UpDot       = dot(up.xyz, lightDir.xyz);
                real    UpDot01     = UpDot * 0.5 + 0.5;
                // return RightL;

                // Notes: sampleing LOD = 0, to ignore image's mip map, which cause aliasing right on the shadow edge.
                real    s01         = SAMPLE_TEXTURE2D_LOD(_ShadowMapTop, sampler_ShadowMapTop,         real2(uv.x, uv.y),  0).r;
                real    s02         = SAMPLE_TEXTURE2D_LOD(_ShadowMapTop, sampler_ShadowMapTop,         real2(-uv.x, uv.y), 0).r;
                real2   lightDataT  = real2(s01,s02);

                real    s03         = SAMPLE_TEXTURE2D_LOD(_ShadowMapMiddle, sampler_ShadowMapMiddle,   real2(uv.x, uv.y),  0).r;
                real    s04         = SAMPLE_TEXTURE2D_LOD(_ShadowMapMiddle, sampler_ShadowMapMiddle,   real2(-uv.x, uv.y), 0).r;
                real2   lightDataM  = real2(s03,s04);

                real    s05         = SAMPLE_TEXTURE2D_LOD(_ShadowMapBottom, sampler_ShadowMapBottom,   real2(uv.x, uv.y),  0).r;
                real    s06         = SAMPLE_TEXTURE2D_LOD(_ShadowMapBottom, sampler_ShadowMapBottom,   real2(-uv.x, uv.y), 0).r;
                real2   lightDataB  = real2(s05,s06);

                real2   lightData;
                real    margin      = _FaceShadowSmoothness;
                real    anchorT     = _ShadowMapTopDown; //0.70; // light from up -> down
                real    anchorB     = -_ShadowMapBottomUp;// -0.70; // light from down -> up
                // return UpDot01;
                if (UpDot > anchorT + margin) 
                { 
                    lightData = lightDataT;
                    //lightData = 0;
                }
                else if (UpDot >= anchorT - margin && UpDot <= anchorT + margin)
                {
                    real x = saturate(invLerp(anchorT - margin, anchorT + margin, UpDot));
                    lightData.x = lerp(lightDataM.x, lightDataT.x, x);
                    lightData.y = lerp(lightDataM.y, lightDataT.y, x);
                    //lightData = 0;
                }
                else if (UpDot < anchorB - margin)
                { 
                    lightData = lightDataB;
                    //lightData = 0;
                }
                else if (UpDot >= anchorB - margin && UpDot <= anchorB + margin) 
                { 
                    real x = saturate(invLerp(anchorB - margin, anchorB + margin, UpDot));
                    lightData.x = lerp(lightDataB.x, lightDataM.x, x);
                    lightData.y = lerp(lightDataB.y, lightDataM.y, x);
                    //lightData = 0;
                }
                else
                {
                    lightData = lightDataM;
                    //lightData = 0;
                }
                lightData           = pow(lightData, power);

                // known issue, light src, with -90/90 degree with right face vector.
                // will result in 0
                real    FL01        = -smoothness * saturate(FrontL * 0.5 + 0.5); // smooth 0 ~ 0.5
                real    L90         = smoothstep(FL01 - RightL, -RightL,    lightData.x); // 0 ~ 1
                real    R90         = smoothstep(FL01 + RightL, RightL,     lightData.y); // 0 ~ 1
                real    occlusion   = step(0, FrontL); // skip dot product, which less then 0, larger 90 degree's light
                real    attenuation = min(L90, R90); // attenuation based on shadow map
                return occlusion * attenuation;
            }

            real3 PreCalAlpha(real4 color4)
            {
                return color4.rgb * color4.a;
            }

            // The fragment shader definition.            
            real4 LitPassFragment(Varyings IN) : SV_Target
            {
                InputData inputData = GetInputData(IN);

                
                // Albedo
                real3 diffuse   = PreCalAlpha(SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, IN.uv_matcap.xy));
                
                // mat cap
                real3 matcapRaw = SAMPLE_TEXTURE2D(_MatCap, sampler_MatCap, IN.uv_matcap.zw).rgb;
                real  matMask   = SAMPLE_TEXTURE2D(_MatCapMask, sampler_MatCapMask, IN.uv_matcap.xy).r;
                real3 matcap    = LerpWhiteTo(saturate(matcapRaw + (1 - matMask)), _MatCapWeight);
                
            #ifdef _MAIN_LIGHT_SHADOWS
                // Main light is the brightest directional light.
                // It is shaded outside the light loop and it has a specific set of variables and shading path
                // so we can be as fast as possible in the case when there's only a single directional light
                // You can pass optionally a shadowCoord (computed per-vertex). If so, shadowAttenuation will be
                // computed.
                Light mainlight = GetMainLight(inputData.shadowCoord, inputData.positionWS, inputData.shadowMask);
            #else
                Light mainlight = GetMainLight();
            #endif

                BiasData ramp   = BiasLight(inputData.normalWS, mainlight, _SkinRampHigh, _SkinRampLow, _SkinRampWeight);
                
                real3 baseColor = lerp(diffuse, diffuse * _Color.rgb, _Color.a);
                real3 shadColor = lerp(diffuse, diffuse * _ShadowMapColor.rgb, _ShadowMapColor.a);
                // return real4(baseColor, 1);
                
                // Shadow map - bias direction
                real sFactor    = FaceShadowMap(IN.uv_matcap.xy, mainlight.direction);
                real3 shadowMap = lerp(shadColor, baseColor, sFactor);
                //return real4(sFactor.rrr,1);

                
                real3 actColor = 0;
                real actNdotL01 = 0;
            #ifdef _ADDITIONAL_LIGHTS
                // Returns the amount of lights affecting the object being renderer.
                // These lights are culled per-object in the forward renderer
                uint additionalLightsCount = GetAdditionalLightsCount();
                for (uint i = 0; i < additionalLightsCount; ++i)
                {
                    // Similar to GetMainLight, but it takes a for-loop index. This figures out the
                    // per-object light index and samples the light buffer accordingly to initialized the
                    // Light struct. If _ADDITIONAL_LIGHT_SHADOWS is defined it will also compute shadows.
                    Light light     = GetAdditionalLight(i, inputData.positionWS);
                    BiasData tmp    = BiasLight(inputData.normalWS, light, _SkinRampAddHigh, _SkinRampAddLow, _SkinRampAddWeight);
                    actColor        += saturate(lerp(0.0, tmp.NdotL * tmp.color * light.distanceAttenuation * light.shadowAttenuation, _FaceShadowMapSubLightBias));
                    actNdotL01      += tmp.NdotL01 * tmp.attenuation;
                }
                //return real4(actColor, 1);
            #endif

                // Calculate skin ramp range.
                //return real4(range.xxx, 1); // debug input range.
                real sampleU    = clamp(ramp.NdotL01 * mainlight.distanceAttenuation + actNdotL01, 0.01, 0.99);
                real2 skinUV    = real2(sampleU, 0.5);
                real3 skinRamp  = SAMPLE_TEXTURE2D(_SkinRamp, sampler_SkinRamp, skinUV).rgb;
                // return real4(sampleU.rrr, 1);
                //return real4(skinRamp,1);
                
                real3 final     = lerp(skinRamp, shadowMap, _ShadowMapWeight);
                final           *= lerp(1.0, mainlight.distanceAttenuation, _FaceShadowMapMainLightBias);
                final           += actColor;
                final           *= matcap;
                //final           += actColor;
                return real4(final,1);
            }
            ENDHLSL
        }
        
        // Used for rendering shadowmaps
        UsePass "Universal Render Pipeline/Lit/ShadowCaster"
        
        // Used for depth prepass
        // If shadows cascade are enabled we need to perform a depth prepass. 
        // We also need to use a depth prepass in some cases camera require depth texture
        // (e.g, MSAA is enabled and we can't resolve with Texture2DMS
        //UsePass "Universal Render Pipeline/Lit/DepthOnly"

        UsePass "Universal Render Pipeline/Lit/DepthNormals"

        // Used for Baking GI. This pass is stripped from build.
        // UsePass "Universal Render Pipeline/Lit/Meta"
    }
    FallBack "Hidden/Universal Render Pipeline/FallbackError"
}