using UnityEngine;

[ExecuteInEditMode]
public class DecalSizeFitter : MonoBehaviour
{
    [SerializeField] Transform m_Size = null;
    [SerializeField] Renderer m_Renderer = null;
    [SerializeField] string m_BoxSize = "_BoxSize";
    private int m_BoxSizeHash = 0;

    [SerializeField] string m_Color = "_Color";
    [SerializeField] Color32 m_Start = Color.white;
    [SerializeField] Color32 m_End = Color.clear;
    private int m_ColorHash = 0;
    private float m_ColorDelta = 0f;
    MaterialPropertyBlock m_Block = null;

    void Update()
    {
        if (m_Renderer != null && m_Block == null)
        {
            m_Block = new MaterialPropertyBlock();
            m_BoxSizeHash = Shader.PropertyToID(m_BoxSize);
            m_ColorHash = Shader.PropertyToID(m_Color);
        }

        if (m_Renderer)
        {
            Vector3 size = m_Size.lossyScale;
            Vector4 v4 = new Vector4(size.x, size.z, 0f, 0f);
            m_Block.SetVector(m_BoxSizeHash, v4);
            Color blend = Color.Lerp(m_Start, m_End, m_ColorDelta);
            m_ColorDelta = Mathf.PingPong(Time.time, 1f);
            m_Block.SetColor(m_ColorHash, blend);

            m_Renderer.SetPropertyBlock(m_Block);
        }
    }
}
