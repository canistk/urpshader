Shader "Kit/Universal Render Pipeline/Triplanar"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color("_Color (default = 1,1,1,1)", color) = (1,1,1,1)
        _Sharpness ("Blend sharpness", Range(1, 64)) = 1

        [Header(Unity Fog)]
        [Toggle(_UnityFogEnable)] _UnityFogEnable("_UnityFogEnable (default = on)", Float) = 1

    }

    // The SubShader block containing the Shader code. 
    SubShader
    {
        // SubShader Tags define when and under which conditions a SubShader block or
        // a pass is executed.
        // https://docs.unity3d.com/Manual/SL-SubShaderTags.html
        Tags {
            "RenderType" = "Opaque"
            "Queue" = "Geometry" // Queue : { Background, Geometry, AlphaTest, Transparent, Overlay }
            "RenderPipeline" = "UniversalPipeline"
            // "DisableBatching" = "True"
        }
        LOD 100
		
        Pass
        {
            // The HLSL code block. Unity SRP uses the HLSL language.
            HLSLPROGRAM
            // This line defines the name of the vertex shader. 
            #pragma vertex vert
            // This line defines the name of the fragment shader. 
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            // due to using ddx() & ddy()
            #pragma target 3.0

            #pragma shader_feature_local _UnityFogEnable

            // The Core.hlsl file contains definitions of frequently used HLSL
            // macros and functions, and also contains #include references to other
            // HLSL files (for example, Common.hlsl, SpaceTransforms.hlsl, etc.).
            // https://github.com/Unity-Technologies/Graphics/blob/master/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

            sampler2D _MainTex;
            CBUFFER_START(UnityPerMaterial)
                float4 _MainTex_ST;
                half4 _Color;
			    float _Sharpness;
            CBUFFER_END

            // The structure definition defines which variables it contains.
            // This example uses the Attributes structure as an input structure in
            // the vertex shader.
            struct Attributes
            {
                // The positionOS variable contains the vertex positions in object
                // space.
                float4 positionOS : POSITION;
                float3 normal : NORMAL;
                float2 uv : TEXCOORD0;
                float3 positionWS : TEXCOORD1;
            };

            struct v2f
            {
                // The positions in this struct must have the SV_POSITION semantic.
                float4 positionCS : SV_POSITION;
                float4 positionWS_Fog : TEXCOORD0;
                float3 normal : NORMAL;
            };            

            // calculate world normal, by multiple inverse matrix of worldToObject
            // that's why the LHS = normal, RHS = World2Object Matrix.
            // https://www.ronja-tutorials.com/post/010-triplanar-mapping/
            float3 TriplanarNormal2World(float3 normal)
            {
                return normalize(mul(normal, (float3x3)unity_WorldToObject));
            }

            float4 GenerateTriplanarUV(float3 worldPos, float3 normal, float4 src_ST)
            {
                // Debug Normal
                // return float4(normal.xyz * 0.5 + 0.5, 1);

                // generate weights from world normals
                float3 weights = normal;
                // show texture on both sides of the object (positive and negative)
                weights = abs(weights);
                // make the transition sharper
				weights = pow(weights, _Sharpness);
				// normalize : make it so the sum of all components is 1
				weights = weights / (weights.x + weights.y + weights.z);

                //calculate UV coordinates for three projections
                float2 uv_front = worldPos.xy * src_ST.xy + src_ST.zw; // TRANSFORM_TEX(worldPos.xy, src);
                float2 uv_side = worldPos.zy * src_ST.xy + src_ST.zw; // TRANSFORM_TEX(worldPos.zy, src);
                float2 uv_top = worldPos.xz * src_ST.xy + src_ST.zw; // TRANSFORM_TEX(worldPos.xz, src);

                //read texture at uv position of the three projections
                float4 col_front = tex2D(_MainTex, uv_front);
                float4 col_side = tex2D(_MainTex, uv_side);
                float4 col_top = tex2D(_MainTex, uv_top);

                //combine weights with projected colors
                col_front *= weights.z;
                col_side *= weights.x;
                col_top *= weights.y;

                //combine the projected colors
                return col_front + col_side + col_top;
            }

            // The vertex shader definition with properties defined in the Varyings 
            // structure. The type of the vert function must match the type (struct)
            // that it returns.
            v2f vert(Attributes IN)
            {
                // Declaring the output object (OUT) with the Varyings struct.
                v2f OUT;

                // VertexPositionInputs contains position in multiple spaces (world, view, homogeneous clip space, ndc)
                // Unity compiler will strip all unused references (say you don't use view space).
                // Therefore there is more flexibility at no additional cost with this struct.
                VertexPositionInputs vertexPositionInput = GetVertexPositionInputs(IN.positionOS.xyz);

                // The TransformObjectToHClip function transforms vertex positions
                // from object space to homogenous clip space.
                OUT.positionCS = vertexPositionInput.positionCS;
                OUT.positionWS_Fog = float4(vertexPositionInput.positionWS, 0);

                // regular unity fog
#if _UnityFogEnable
                OUT.positionWS_Fog = float4(vertexPositionInput.positionWS, ComputeFogFactor(OUT.positionCS.z));
#endif

                OUT.normal = TriplanarNormal2World(IN.normal);
                return OUT;
            }


            // The fragment shader definition.            
            float4 frag(v2f IN) : SV_Target
            {
                float4 col = GenerateTriplanarUV(IN.positionWS_Fog.xyz, IN.normal, _MainTex_ST);
                
                col *= _Color;

#if _UnityFogEnable
                // Mix the pixel color with fogColor. You can optionaly use MixFogColor to override the fogColor
                // with a custom one.
                col.rgb = MixFog(col.rgb, IN.positionWS_Fog.w);
#endif
                return col;
            }
            ENDHLSL
        }
    }
}