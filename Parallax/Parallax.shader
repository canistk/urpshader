/**
    https://learnopengl.com/Advanced-Lighting/Parallax-Mapping
    https://www.habrador.com/tutorials/shaders/3-parallax-mapping/
 */
Shader "Kit/Universal Render Pipeline/Parallax"
{
    Properties
    {
        [MainColor] _BaseColor("Color", Color) = (0.5,0.5,0.5,1)
        [MainTexture][noscaleOffset] _BaseMap("Albedo", 2D) = "white" {}
        _NormalScale("Normal Map Scale", Range(0.0, 1.0)) = 1.0
        [Normal][noscaleOffset] _NormalMap("Normal Map", 2D) = "bump" {}

        _DepthMap("Depth Map", 2D) = "white" {}
        _Height ("Marching in texels and dropping our y-value accordingly", Float) = 1.0

    }

    SubShader
    {
        Tags{
            "RenderType" = "Transparent"
            "Queue" = "Transparent" // Queue : { Background, Geometry, AlphaTest, Transparent, Overlay }
            "RenderPipeline" = "UniversalRenderPipeline"
            "IgnoreProjector" = "True"
        }
        // https://docs.unity3d.com/Manual/SL-ShaderLOD.html
        LOD 300
        Cull Back
        ZWrite On
        // ZTest Less    
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            // "Lightmode" tag must be "UniversalForward" or not be defined in order for
            // to render objects.
            Name "Parallax"
            Tags {
                "LightMode" = "UniversalForward"
            }

            HLSLPROGRAM
            #pragma target 3.0

            //--------------------------------------
            // GPU Instancing
            #pragma multi_compile_instancing

            #pragma vertex      LitPassVertex
            #pragma fragment    LitPassFragment

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            
            struct Attributes
            {
                real4 positionOS    : POSITION;
                real3 normalOS      : NORMAL;
                real4 tangentOS     : TANGENT;
                real2 uv            : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct Varyings
            {
                real4 positionCS                : SV_POSITION;
                real3 positionWS                : TEXCOORD0;
                real3 positionOS                : TEXCOORD1;
                real3 normalWS                  : TEXCOORD2;
                real3 tangentWS                 : TEXCOORD3;
                real3 bitangentWS               : TEXCOORD4;
                real2 uv                        : TEXCOORD5;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                UNITY_VERTEX_OUTPUT_STEREO
            };

            TEXTURE2D(_BaseMap);            SAMPLER(sampler_BaseMap);
            TEXTURE2D(_DepthMap);           SAMPLER(sampler_DepthMap);
            TEXTURE2D(_NormalMap);          SAMPLER(sampler_NormalMap);
            // In order to support VR & GPU Instancing
            UNITY_INSTANCING_BUFFER_START(UnityPerMaterial)
                UNITY_DEFINE_INSTANCED_PROP(real4, _BaseMap_ST);
                UNITY_DEFINE_INSTANCED_PROP(real4, _NormalMap_ST);
                UNITY_DEFINE_INSTANCED_PROP(real4, _DepthMap_ST);
                UNITY_DEFINE_INSTANCED_PROP(real4, _BaseColor);
                UNITY_DEFINE_INSTANCED_PROP(real, _Height);
                UNITY_DEFINE_INSTANCED_PROP(real, _NumStep);
            UNITY_INSTANCING_BUFFER_END(UnityPerMaterial)
            
            Varyings LitPassVertex(Attributes IN)
            {
                Varyings OUT = (Varyings)0;
                
                // https://docs.unity3d.com/Manual/SinglePassInstancing.html // Support in 2020.3
                UNITY_SETUP_INSTANCE_ID(IN);
                //UNITY_INITIALIZE_OUTPUT(Varyings, OUT); // Support in 2020.3
                // https://docs.unity3d.com/Manual/SinglePassStereoRendering.html
                // https://docs.unity3d.com/Manual/Android-SinglePassStereoRendering.html
                UNITY_TRANSFER_INSTANCE_ID(IN, OUT); // necessary only if you want to access instanced properties in the fragment Shader.
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT); // VR support - Single-Pass Stereo Rendering for Android

                VertexPositionInputs vertexInput = GetVertexPositionInputs(IN.positionOS.xyz);
                VertexNormalInputs normalInput = GetVertexNormalInputs(IN.normalOS, IN.tangentOS);

                // TRANSFORM_TEX is the same as the old shader library.
                OUT.uv.xy = IN.uv; // TRANSFORM_TEX(IN.uv, _BaseMap);

                // We just use the homogeneous clip position from the vertex input
                OUT.positionCS = vertexInput.positionCS;
                OUT.positionOS = IN.positionOS.xyz;
                OUT.positionWS = vertexInput.positionWS;

                // Here comes the flexibility of the input structs.
                // In the variants that don't have normal map defined
                // tangentWS and bitangentWS will not be referenced and
                // GetnormalInputs is only converting normal
                // from object to world space
                OUT.normalWS = normalInput.normalWS;
                OUT.tangentWS = normalInput.tangentWS;
                OUT.bitangentWS = normalInput.bitangentWS;
                return OUT;
            }

            real MySampleDepthMap(real2 uv)
            {
                //const float threshold = 0.0009765625;
                //float2 dx = clamp(ddx(uv), -threshold, threshold);
                //float2 dy = clamp(ddy(uv), -threshold, threshold);
                //return SAMPLE_TEXTURE2D_GRAD(_DepthMap, sampler_DepthMap, uv * _DepthMap_ST.xy + _DepthMap_ST.zw, dx, dy).g;
                return SAMPLE_TEXTURE2D_LOD(_DepthMap, sampler_DepthMap, uv * _DepthMap_ST.xy + _DepthMap_ST.zw, 0).r;
            }

            // Get the texture position by interpolation between the position where we hit terrain and the position before
            real2 GetWeightedUV(real3 rayPos, real3 rayDir, real stepDistance)
            {
                //Move one step back to the position before we hit terrain
			    real3 oldPos = rayPos - stepDistance * rayDir;
                real oldHeight = MySampleDepthMap(oldPos.xz);
                //Always positive
                real oldDistToTerrain = abs(oldHeight - oldPos.y);
                real currentHeight = MySampleDepthMap(rayPos.xz);
                //Always negative
                real currentDistToTerrain = rayPos.y - currentHeight;
                real weight = currentDistToTerrain / (currentDistToTerrain - oldDistToTerrain);

			    //Calculate a weighted texture coordinate
			    //If height is -2 and oldHeight is 2, then weightedTex is 0.5, which is good because we should use 
			    //the exact middle between the coordinates
                real2 weightedTexPos = oldPos.xz * weight + rayPos.xz * (1 - weight);
			    return weightedTexPos;
            }

            // https://learnopengl.com/Advanced-Lighting/Parallax-Mapping
            real2 ParallaxMapping(real2 uv, real3 localViewDir)
            {
                const float minLayers = 8.0;
                const float maxLayers = 32.0;
                const float3 forward = float3(0.0, 0.0, 1.0);
                float numLayers = lerp(maxLayers, minLayers, saturate(dot(forward, localViewDir)));
                
                real layerDepth = 1.0 / numLayers;
                real layerHeight = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _Height);
                // the amount to shift the texture coordinates per layer (from vector P)
                real2 P = localViewDir.xz * layerHeight;
                real2 stepUV = P / numLayers;

                
                if (layerHeight == 0)
                    return uv;

                real2 currentUV = uv;
                real currentDepth = MySampleDepthMap(currentUV);
                real oldDepth = currentDepth;
                
                for (real currentLayerDepth = 0.0; currentLayerDepth < currentDepth && currentLayerDepth < 1.0; currentLayerDepth += layerDepth)
                {
                    // shift texture coordinates along direction of P
                    currentUV       += stepUV;

                    // get depthmap value at current texture coordinates
                    currentDepth    = MySampleDepthMap(currentUV);
                    
                    // stop iteration when out of range.
                    if (currentUV.x > 1.0 || currentUV.y > 1.0 || currentUV.x < 0.0 || currentUV.y < 0.0)
                        discard;

                }

                return currentUV;
            }

            real4 LitPassFragment(Varyings IN) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID(IN); // necessary only if any instanced properties are going to be accessed in the fragment Shader.
                UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(IN); // VR support - Single-Pass Stereo Rendering for Android

                // Method 3
                //real3 localViewDir = GetObjectSpaceNormalizeViewDir(IN.positionOS); // Support in 2021.3

                // Method 2
                //real3 V = normalize(GetCameraPositionWS() - IN.positionWS);
                //real3 localViewDir = TransformWorldToObjectDir(V);
                
                real3 cameraPosOS = TransformWorldToObject(GetCameraPositionWS());
                real3 localViewDir = normalize(cameraPosOS - IN.positionOS);

                real2 uv = IN.uv * _BaseMap_ST.xy + _BaseMap_ST.zw;
                real2 parallaxUV = ParallaxMapping(uv, localViewDir);
                real4 diffuse = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, parallaxUV);
                real alpha = diffuse.a;

                real4 baseColor = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _BaseColor);
                real3 final = baseColor.rgb * diffuse.rgb * diffuse.a;
                return real4(final, alpha);
            }
            ENDHLSL
        }

    }

    // Uses a custom shader GUI to display settings. Re-use the same from Lit shader as they have the
    // same properties.
    // CustomEditor "UnityEditor.Rendering.Universal.ShaderGUI.LitShader"
    // CustomEditor "URPLitShaderGUI"
}