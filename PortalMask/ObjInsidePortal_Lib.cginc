#include "UnityCG.cginc"
sampler2D _MainTex;
float4 _MainTex_ST;
float4 _Color;
float4 _BackColor;
float4 _PlaneNormal;

struct appdata
{
	float4 vertex : POSITION;
	float3 normal : NORMAL;
	float2 uv : TEXCOORD0;
};

struct v2f
{
	float4 vertex : SV_POSITION;
	float2 uv : TEXCOORD0;
	float3 wpos : TEXCOORD1;
	float3 normal : TEXCOORD2;
	float vertdis : TEXCOORD3;
};

v2f internal_vert_main (appdata v)
{
	v2f o;
    o.normal = v.normal;
	o.uv = TRANSFORM_TEX(v.uv, _MainTex);
	o.wpos = mul(unity_ObjectToWorld, v.vertex);
    o.vertex = UnityObjectToClipPos(v.vertex);
	
	float3 planeNormal = _PlaneNormal.xyz;
	float planeDis = _PlaneNormal.w;
	o.vertdis = dot(o.wpos, planeNormal) + planeDis;
	// project this vertex on plane.
	// float4 project = float4(o.wpos + (planeNormal * -o.vertdis), 1);
	// back to World > object space -> clip space
	// o.projectvertex = UnityWorldToClipPos(project);
    return o;
}

v2f internal_vert_edge (appdata v)
{
	v2f o;
    o.normal = v.normal;
	o.uv = TRANSFORM_TEX(v.uv, _MainTex);
	o.wpos = mul(unity_ObjectToWorld, v.vertex);
	o.vertex = UnityObjectToClipPos(v.vertex);
	o.vertdis = dot(o.wpos, _PlaneNormal.xyz) + _PlaneNormal.w;
	return o;
}

// the incoming value should be vector3(xyz) + distance from origin(w).
// _Plane.xyz = Plane Normal (Unit vector)
// _Plane.w = Plane world position along the normal direction based on origin(0,0,0).
// therefore we can calculate the current vertex on which side of the plane by it's sign.
fixed4 internal_frag (v2f i, float face, int sign)
{
	//fixed4 red = fixed4(1,0,0,1);
	//fixed4 green = fixed4(0,1,0,1);
	//fixed4 blue = fixed4(0,0,1,1);
	//fixed4 yellow = fixed4(1,1,0,1);
	//fixed4 clear = fixed4(0,0,0,0);
	//fixed4 dim = fixed4(.1,.1,.1,1);
	fixed4 color = tex2D(_MainTex, i.uv);
	if (sign > 0)
	{
		if (i.vertdis < 0)
            discard;
    }
	else
	{
		if (i.vertdis >= 0)
			discard;
	}
	// sample the texture
	if (face > 0.5)
		return _Color * color;
	else
		return _BackColor *color;
}

float4 ProjectVectexOnPlane(float4 vertex, float3 planePos, float3 planeNormal)
{
	float3 worldPos = mul(unity_ObjectToWorld, vertex);
	// distance between this vertex & _Plane.normal
	float vertexDistance = dot(worldPos, planeNormal);
	// project this vertex on plane.
	float4 project = float4(worldPos + planePos + (planeNormal * -vertexDistance), 1);
	// back to object space -> clip space
	float4 obj = mul(unity_WorldToObject, project);
	return UnityObjectToClipPos(obj);
}