// #define SEND_POS
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class PlaneCoordinate : MonoBehaviour
{
    [SerializeField] Renderer m_Renderer = null;
    
    [SerializeField] Transform m_Plane = null;
    [SerializeField] Vector3 m_Normal = Vector3.up;

#if SEND_POS
    [SerializeField] string m_PlanePos = "_PlanePos";
    private int m_PosHash = 0;
#endif
    [SerializeField] string m_PlaneNormal = "_PlaneNormal";
    private int m_NormalHash = 0;
    private MaterialPropertyBlock m_Block = null;

    [Header("Debug")]
    public bool m_Debug = false;
    public Vector3 samplePoint = Vector3.one * 0.5f;

    void Update()
    {
        if (m_Renderer != null && m_Block == null)
        {
            m_Block = new MaterialPropertyBlock();
            m_NormalHash = Shader.PropertyToID(m_PlaneNormal);
#if SEND_POS
            m_PosHash = Shader.PropertyToID(m_PlanePos);
#endif
        }

        if (m_Renderer && m_Plane && m_Normal != Vector3.zero)
        {
            // normalize vector
            Vector3 planeNormal = m_Plane.rotation * m_Normal.normalized;
            Plane plane = new Plane(planeNormal, m_Plane.position);

            // Normal = transform.up (unit vector)
            // World Position is a vector from world position to origin,
            // AKA vector = (pos) - (0,0,0)
            // calculate dot between world position & Normal 
            // the result will be the distance project along the normal in origin (0,0,0);
            Vector4 planeRepresent = new Vector4(plane.normal.x, plane.normal.y, plane.normal.z, plane.distance);
            
            m_Block.SetVector(m_NormalHash, planeRepresent);
#if SEND_POS
            Vector3 pPos = m_Plane.position;
            m_Block.SetVector(m_PosHash, pPos);
#endif
            m_Renderer.SetPropertyBlock(m_Block);
        }
    }

    private void OnDrawGizmos()
    {
        if (m_Debug && m_Plane && m_Renderer)
        {
            Color old = Gizmos.color;
            Gizmos.color = Color.yellow;
            Vector3 sample;
            sample = m_Renderer.transform.TransformPoint(samplePoint);
            Gizmos.DrawWireSphere(sample, 0.05f);
            Vector3 planePos = m_Plane.position;
            Vector3 planeNormal = m_Plane.rotation * m_Normal.normalized;
            float dis = Vector3.Dot(sample - planePos, planeNormal);
            Vector3 project = sample + planeNormal * -dis;

            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(project, 0.1f);

            Gizmos.color = old;
        }
    }
}
