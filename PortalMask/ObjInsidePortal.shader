Shader "Kit/Universal Render Pipeline/PortalMask/ObjInsidePortal"
{
    Properties
    {
		[Header(Stencil Masking)]
        // https://docs.unity3d.com/ScriptReference/Rendering.CompareFunction.html
        _StencilRef("_StencilRef", Float) = 5
		[Toggle(_EnableProjection)] _EnableProjection("Enable Projection (default = off)", Float) = 0

		_MainTex ("Texture", 2D) = "white" {}
		_Color ("Color", Color) = (0.5,0.5,0.5)
		_BackColor ("Back Color", Color) = (0.5,0.5,0.5)
		
		[Header(Plane)]
		_PlaneNormal("Plane Normal", Vector) = (0,1,0,0)
		_PlanePos("Plane Position", Vector) = (0,0,0,0)
		
    }
    SubShader
    {
        Tags {
			"RenderType"="Opaque"
			"RenderPipeline"="UniversalRenderPipeline"
			"Queue" = "Transparent+2"
		}
        LOD 100
        Pass
        {
			Name "ForwardLit"
			Tags {
				"LightMode" = "UniversalForward"
			}
			Cull Off
            HLSLPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#include "ObjInsidePortal_Lib.cginc"
			
			v2f vert (appdata v)
			{
				return internal_vert_main(v);
			}

			fixed4 frag(v2f i, float face : VFACE) : SV_Target
			{
				return internal_frag(i,face,1);
			}
            ENDHLSL
        }

		Pass
        {
			Name "InsidePortal"
			Tags {
				"LightMode" = "SRPDefaultUnlit"
			}
            Stencil
            {
                Ref [_StencilRef]
                Comp Equal
            }
            HLSLPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#include "ObjInsidePortal_Lib.cginc"
			
			v2f vert (appdata v)
			{
				return internal_vert_edge(v);
			}

			fixed4 frag(v2f i, float face : VFACE) : SV_Target
			{
				return internal_frag(i,face,0);
			}
            ENDHLSL
        }
    }
}