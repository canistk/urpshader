Shader "Kit/Universal Render Pipeline/FullScreenRender"
{
    Properties
    {
        //_MainMap ("Main Texture", 2D) = "white" {}
        //_BaseMap ("Texture", 2D) = "white" {}
        _BaseColor("_Color (default = 1,1,1,1)", color) = (1,1,1,1)
    }

    SubShader
    {
        Tags
        {
            "RenderType" = "Opaque"
            "RenderPipeline" = "UniversalRenderPipeline"
            "ForceNoShadowCasting" = "True"
            "IgnoreProjector" = "True"
            "PreviewType" = "Plane"
            "DisableBatching" = "True"
        }
        LOD 100
        ZWrite Off
        Cull Off
        
        Pass
        {
    
            HLSLPROGRAM
            #pragma vertex VertexPass
            #pragma fragment FragmentPass

            // Required by all Universal Render Pipeline shaders.
            // It will include Unity built-in shader variables (except the lighting variables)
            // (https://docs.unity3d.com/Manual/SL-UnityShaderVariables.html
            // It will also include many utilitary functions. 
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            
            struct Attributes
            {
                real4 positionOS    : POSITION;
                real2 uv            : TEXCOORD0;
            };

            struct Varyings
            {
                real4 positionCS    : SV_POSITION;
                real2 uv            : TEXCOORD0;
            };

            TEXTURE2D(_BaseMap);            SAMPLER(sampler_BaseMap);
            TEXTURE2D(_MainMap);            SAMPLER(sampler_MainMap);
            UNITY_INSTANCING_BUFFER_START(UnityPerMaterial)
                UNITY_DEFINE_INSTANCED_PROP(half4, _BaseMap_ST);
                UNITY_DEFINE_INSTANCED_PROP(half4, _BaseColor);
            UNITY_INSTANCING_BUFFER_END(UnityPerMaterial)

            
            Varyings VertexPass (Attributes i)
            {
                Varyings o;
                
                // in screen space, OS == CS,
                // assume we draw on full screen quad.
                VertexPositionInputs vertexInput = GetVertexPositionInputs(i.positionOS.xyz);
                
                o.positionCS    = vertexInput.positionCS;
                o.uv            = TRANSFORM_TEX(i.uv, _BaseMap);

                //https://docs.unity3d.com/cn/2019.4/Manual/SL-PlatformDifferences.html
                //#if UNITY_UV_STARTS_AT_TOP
                //o.positionCS.y = 1 - o.positionCS.y;
                //#endif
                return o;
            }

            real4 FragmentPass (Varyings input) : SV_Target
            {
                real4 col = (real4) 1.0;
                col.rgb = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, input.uv).rgb;
                col.rgb = lerp(_BaseColor.rgb, col.rgb, _BaseColor.a);
                // col.gb -= 0.5;
                return col;
            }
            ENDHLSL
        }
    }
}
