using Kit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

namespace Kit
{
    public class FullScreenRender : MonoRenderPass
    {
        public RenderPassEvent m_RenderPassEvent = RenderPassEvent.AfterRenderingTransparents;
        public override RenderPassEvent renderPassEvent => m_RenderPassEvent;

        public Material m_Material;

        [SerializeField] CameraType m_RenderCamera = CameraType.Game | CameraType.SceneView;
        [SerializeField, Min(0f)] float m_ClipBuffer = 0.0001f;

        public override RenderTargetIdentifier cameraColorTarget { get; set; }
        private static Mesh m_FullScreenTriangle;

        public override void OnCameraPreCull(ScriptableRenderer renderer, in CameraData cameraData)
        {

        }

        public override void Configure(ScriptableRenderPass render, CommandBuffer cmd, RenderTextureDescriptor cameraTextureDescriptor)
        {
            PrepareFullScreenMesh();
        }

        public override void Execute(ScriptableRenderPass render, ScriptableRenderContext context, ref RenderingData renderingData)
        {
            if (!m_Material)
                return;

            if ((renderingData.cameraData.cameraType & m_RenderCamera) == 0)
                return;

            var cam     = renderingData.cameraData.camera;
            var matrix  = cam.transform.localToWorldMatrix;
            var offset  = Matrix4x4.Translate(Vector3.forward * (cam.nearClipPlane + m_ClipBuffer));
            var cmd     = CommandBufferPool.Get(GetType().Name);

            cmd.DrawMesh(m_FullScreenTriangle, matrix * offset, m_Material);
            context.ExecuteCommandBuffer(cmd);
            cmd.Clear();
            CommandBufferPool.Release(cmd);
        }

        public override void OnCameraCleanup(ScriptableRenderPass render, CommandBuffer cmd)
        {
        }

        private void PrepareFullScreenMesh()
        {
            if (m_FullScreenTriangle != null)
                return;

            m_FullScreenTriangle = new Mesh()
            {
                name = "PostProcessMesh",
                vertices = new Vector3[] {
                    new Vector3(-1, -1, 0),
                    new Vector3( 3, -1, 0),
                    new Vector3(-1,  3, 0),
                },
                triangles = new int[] { 0, 1, 2 }
            };
            m_FullScreenTriangle.UploadMeshData(true);
        }
    }
}