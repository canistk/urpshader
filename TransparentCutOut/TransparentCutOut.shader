
Shader "Kit/Universal Render Pipeline/TransparentCutOut"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color1("_Color (default = 1,1,1,1)", color) = (1,1,1,1)
        _Color2("_Color (default = 1,1,1,1)", color) = (1,1,1,1)
        _Color3("_Color (default = 1,1,1,1)", color) = (1,1,1,1)
    }

    // The SubShader block containing the Shader code. 
    SubShader
    {
        // SubShader Tags define when and under which conditions a SubShader block or
        // a pass is executed.
        // https://docs.unity3d.com/Manual/SL-SubShaderTags.html
        Tags {
            "RenderQueue"       = "AlphaTest"
            "RenderType"        = "TransparentCutout"
            "Queue"             = "AlphaTest" // Queue : { Background, Geometry, AlphaTest, Transparent, Overlay }
            "RenderPipeline"    = "UniversalPipeline"
            "IgnoreProjector"   = "True"
            "DisableBatching"   = "True"
        }
        LOD 300
		
        Pass
        {
            Name "Opaque"
            Tags
            {
                "LightMode" = "UniversalForward"
            }
            Cull Off
            ZWrite On
            ZTest LEqual
            AlphaToMask On
            // Blend SrcAlpha OneMinusSrcAlpha
            // ColorMask 0
            AlphaTest Less 0.5

            HLSLPROGRAM
            #pragma vertex UnlitVertexShader
            #pragma fragment UnlitFragmentShader
         
            // due to using ddx() & ddy()
            #pragma target 3.0

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            
            TEXTURE2D(_MainTex);        SAMPLER(sampler_MainTex);
            UNITY_INSTANCING_BUFFER_START(UnityPerMaterial)
                UNITY_DEFINE_INSTANCED_PROP(half4, _MainTex_ST)
                UNITY_DEFINE_INSTANCED_PROP(half4, _Color1)
                UNITY_DEFINE_INSTANCED_PROP(half4, _Color2)
                UNITY_DEFINE_INSTANCED_PROP(half4, _Color3)
            UNITY_INSTANCING_BUFFER_END(UnityPerMaterial)

            struct Attributes
            {
                real4   positionOS  : POSITION;
                real3   normalOS    : NORMAL;
                real4   tangentOS   : TANGENT;
                real2   uv          : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct Varyings
            {
                real4   positionCS  : SV_POSITION;
                real3   positionWS  : TEXCOORD0;
                real3   tangentWS   : TEXCOORD1;
                real3   bitangentWS : TEXCOORD2;
                real3   normalWS    : TEXCOORD3;
                real2   uv          : TEXCOORD4;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                UNITY_VERTEX_OUTPUT_STEREO
            };

            Varyings UnlitVertexShader(Attributes IN)
            {
                // Declaring the output object (OUT) with the Varyings struct.
                Varyings OUT;

                UNITY_SETUP_INSTANCE_ID(IN);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT); // VR support - Single-Pass Stereo Rendering for Android
                UNITY_TRANSFER_INSTANCE_ID(IN, OUT); // necessary only if you want to access instanced properties in the fragment Shader.
                VertexPositionInputs vertexPositionInput    = GetVertexPositionInputs(IN.positionOS.xyz);
                VertexNormalInputs normalInput              = GetVertexNormalInputs(IN.normalOS, IN.tangentOS);

                // The TransformObjectToHClip function transforms vertex positions
                // from object space to homogenous clip space.
                OUT.positionCS  = vertexPositionInput.positionCS;
                // OUT.positionNDC = vertexPositionInput.positionNDC;
                // OUT.positionVS = vertexPositionInput.positionVS;
                OUT.positionWS  = vertexPositionInput.positionWS;

                OUT.tangentWS   = normalInput.tangentWS;
                OUT.bitangentWS = normalInput.bitangentWS;
                OUT.normalWS    = normalInput.normalWS;
                OUT.uv          = TRANSFORM_TEX(IN.uv, _MainTex);

                return OUT;
            }

            // The fragment shader definition.            
            half4 UnlitFragmentShader(Varyings IN) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID(IN); // necessary only if any instanced properties are going to be accessed in the fragment Shader.
                UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(IN); // VR support - Single-Pass Stereo Rendering for Android
                // Read GPU instancing data
                float4 i_color = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _Color1);

                // Defining the color variable and returning it.
                half4 col = i_color * SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, IN.uv);
                clip(col.a - 0.5);
                return col;
            }
            ENDHLSL
        }
        
        // Used for rendering shadowmaps
        Pass
        {
            Name "ShadowCaster"
			Tags{"LightMode" = "ShadowCaster"}

			ZWrite On
			ZTest LEqual
			ColorMask 0
			Cull Back

			HLSLPROGRAM
			// Required to compile gles 2.0 with standard srp library
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			//--------------------------------------
			// GPU Instancing
			#pragma multi_compile_instancing
			#pragma shader_feature _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A

			#pragma vertex ShadowVertex
			#pragma fragment ShadowFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

			struct Attributes
			{
				float4 positionOS   : POSITION;
				float3 normalOS     : NORMAL;
				float2 uv           : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float2 uv           : TEXCOORD0;
				float4 positionCS   : SV_POSITION;

				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			TEXTURE2D(_MainTex);        SAMPLER(sampler_MainTex);
            UNITY_INSTANCING_BUFFER_START(UnityPerMaterial)
                UNITY_DEFINE_INSTANCED_PROP(half4, _MainTex_ST)
			UNITY_INSTANCING_BUFFER_END(UnityPerMaterial)

			Varyings ShadowVertex(Attributes input)
			{
				Varyings output;
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				float3 positionWS = TransformObjectToWorld(input.positionOS.xyz);
				float3 normalWS = TransformObjectToWorldNormal(input.normalOS.xyz);
				float4 positionCS = TransformWorldToHClip(ApplyShadowBias(positionWS, normalWS, _MainLightPosition.xyz));

				output.positionCS = positionCS;
				output.uv = TRANSFORM_TEX(input.uv, _MainTex);

				return output;
			}

			half4 ShadowFragment(Varyings input) : SV_TARGET
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				half4 albedoAlpha = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv);

				// alpha test
				half alpha = albedoAlpha.a;
				clip(alpha - 0.5);

				return 0;
			}
			ENDHLSL
        }
    }
}