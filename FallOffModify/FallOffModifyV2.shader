Shader "Kit/Universal Render Pipeline/FallOffModifyV2"
{
    Properties
    {
        _MainTex ("Main Texture", 2D) = "white" {}
        
        [Space(10)]
        
        [Header(HighLight)]
        _LightColor("Light Color", Color) = (1,1,1,1)
        _P1 ("P1", Range(-1.0,1.0)) = 1.0
        _P2 ("P2", Range(-1.0,1.0)) = 0.25
        _EdgeColor("Edge Color", Color) = (1,0,0,1)
        _P3 ("P3", Range(-1.0,1.0)) = -0.25
        _P4 ("P4", Range(-1.0,1.0)) = -1.0
        _ShadowColor("Shadow Color", Color) = (0,0,0,1)
    }

    SubShader
    {
        // https://docs.unity3d.com/Manual/SL-SubShaderTags.html
        Tags {
            "RenderType" = "Opaque"
            "Queue" = "Geometry" // Queue : { Background, Geometry, AlphaTest, Transparent, Overlay }
            "RenderPipeline" = "UniversalPipeline"
            // "DisableBatching" = "True"
        }
        LOD 100
		
        Pass
        {
            HLSLPROGRAM
            #pragma vertex LitPassVertex
            #pragma fragment LitPassFragment
            #pragma multi_compile _ _MAIN_LIGHT_SHADOWS
            #pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
            #pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
            // https://github.com/Unity-Technologies/Graphics/blob/master/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            // Include this if you are doing a lit shader. This includes lighting shader variables,
            // lighting and shadow functions
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

            #include "../ShaderLibrary/KitShader.hlsl"
            
            #define MY_TEXTURE(name, uv) SAMPLE_TEXTURE2D(name, sampler##name, uv.xy * name##_ST.xy + name##_ST.zw)

            //TEXTURE2D(_BaseMap);      SAMPLER(sampler_BaseMap);
            TEXTURE2D(_MainTex);        SAMPLER(sampler_MainTex);
            
            UNITY_INSTANCING_BUFFER_START(UnityPerMaterial)
                UNITY_DEFINE_INSTANCED_PROP(real4,  _MainTex_ST);

                UNITY_DEFINE_INSTANCED_PROP(real4,  _LightColor);
                UNITY_DEFINE_INSTANCED_PROP(real4,  _ShadowColor);
                UNITY_DEFINE_INSTANCED_PROP(real4,  _EdgeColor);
                UNITY_DEFINE_INSTANCED_PROP(real,   _P1);
                UNITY_DEFINE_INSTANCED_PROP(real,   _P2);
                UNITY_DEFINE_INSTANCED_PROP(real,   _P3);
                UNITY_DEFINE_INSTANCED_PROP(real,   _P4);
                
            UNITY_INSTANCING_BUFFER_END(UnityPerMaterial)
            
            struct Attributes
            {
                // The positionOS variable contains the vertex positions in object
                // space.
                real4 positionOS    : POSITION;
                real3 normalOS      : NORMAL;
                real2 uv            : TEXCOORD0;
                real4 color         : COLOR;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct Varyings
            {
                // The positions in this struct must have the SV_POSITION semantic.
                real4 positionCS    : SV_POSITION;
                real4 color         : COLOR;
                real3 normalWS      : NORMAL;
                real2 uv            : TEXCOORD0;
                real3 positionWS    : TEXCOORD1;
                real3 vertexLight   : TEXCOORD2;
#ifdef _MAIN_LIGHT_SHADOWS
                real4 shadowCoord   : TEXCOORD3; // compute shadow coord per-vertex for the main light
#endif
                UNITY_VERTEX_INPUT_INSTANCE_ID
                UNITY_VERTEX_OUTPUT_STEREO
            };            

            InputData GetInputData(Varyings IN)
            {
                InputData inputData;
                inputData.positionWS        = IN.positionWS;
                inputData.normalWS          = IN.normalWS;
                inputData.viewDirectionWS   = SafeNormalize(GetCameraPositionWS() - IN.positionWS);
                inputData.fogCoord          = ComputeFogFactor(IN.positionCS.z);
                inputData.vertexLighting    = IN.vertexLight;
#if _MAIN_LIGHT_SHADOWS
                inputData.shadowCoord       = IN.shadowCoord;
#else
                inputData.shadowCoord       = 0;
#endif
                inputData.normalizedScreenSpaceUV = GetNormalizedScreenSpaceUV(IN.positionCS);
                // To ensure backward compatibility we have to avoid using shadowMask input, as it is not present in older shaders
#if defined(SHADOWS_SHADOWMASK) && defined(LIGHTMAP_ON)
                real4 shadowMask = inputData.shadowMask;
#elif !defined (LIGHTMAP_ON)
                real4 shadowMask = unity_ProbesOcclusion;
#else
                real4 shadowMask = real4(1, 1, 1, 1);
#endif
                inputData.shadowMask        = shadowMask;
                return inputData;
            }

            Varyings LitPassVertex(Attributes IN)
            {
                // https://docs.unity3d.com/Manual/SinglePassInstancing.html // Support in 2020.3
                UNITY_SETUP_INSTANCE_ID(IN);
                //UNITY_INITIALIZE_OUTPUT(Varyings, OUT); // Support in 2020.3
                // https://docs.unity3d.com/Manual/SinglePassStereoRendering.html
                // https://docs.unity3d.com/Manual/Android-SinglePassStereoRendering.html
                UNITY_TRANSFER_INSTANCE_ID(IN, OUT); // necessary only if you want to access instanced properties in the fragment Shader.
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT); // VR support - Single-Pass Stereo Rendering for Android

                Varyings OUT;
                VertexPositionInputs vertexInput = GetVertexPositionInputs(IN.positionOS.xyz);
                
                OUT.positionCS = vertexInput.positionCS;
                OUT.positionWS = vertexInput.positionWS;
                OUT.normalWS = TransformObjectToWorldNormal(IN.normalOS);
                OUT.color = IN.color;
                OUT.uv = IN.uv;
                OUT.vertexLight = VertexLighting(OUT.positionWS, OUT.normalWS);
#ifdef _MAIN_LIGHT_SHADOWS
                // shadow coord for the main light is computed in vertex.
                // If cascades are enabled, LWRP will resolve shadows in screen space
                // and this coord will be the uv coord of the screen space shadow texture.
                // Otherwise LWRP will resolve shadows in light space (no depth pre-pass and shadow collect pass)
                // In this case shadowCoord will be the position in light space.
                OUT.shadowCoord = GetShadowCoord(vertexInput);
#endif
                return OUT;
            }

            real3 FallOffModify(InputData inputData, Light light,
                real3 lightColor, real3 edgeColor, real3 shadowColor,
                real p1, real p2, real p3, real p4)
            {
                real NdotL = dot(inputData.normalWS, light.direction);
                
                // smoothstep(min, max, t)
                real f1 = smoothstep(min(p1,p2), max(p1,p2), NdotL);
                real f3 = smoothstep(min(p3,p4), max(p3,p4), NdotL);
                real m1 = min(p1, p2);
                real m2 = max(p3, p4);
                real f2 = smoothstep(min(m1,m2), max(m1,m2), NdotL);

                real3 modify = saturate(light.color * lightColor * f1) + saturate(edgeColor * f2) + saturate(shadowColor * f3);
                modify *= light.distanceAttenuation * light.shadowAttenuation;
                return modify;
            }

            // The fragment shader definition.            
            real4 LitPassFragment(Varyings IN) : SV_Target
            {
                InputData inputData = GetInputData(IN);
                
            #ifdef _MAIN_LIGHT_SHADOWS
                // Main light is the brightest directional light.
                // It is shaded outside the light loop and it has a specific set of variables and shading path
                // so we can be as fast as possible in the case when there's only a single directional light
                // You can pass optionally a shadowCoord (computed per-vertex). If so, shadowAttenuation will be
                // computed.
                Light mainlight = GetMainLight(inputData.shadowCoord, inputData.positionWS, inputData.shadowMask);
            #else
                Light mainlight = GetMainLight();
            #endif

                real4 albedo    = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, IN.uv);
                real3 modify    = FallOffModify(inputData, mainlight, _LightColor.rgb, _EdgeColor.rgb, _ShadowColor.rgb, _P1, _P2, _P3, _P4);
                real3 color     = albedo * IN.color * modify;

            #ifdef _ADDITIONAL_LIGHTS
                // Returns the amount of lights affecting the object being renderer.
                // These lights are culled per-object in the forward renderer
                uint additionalLightsCount = GetAdditionalLightsCount();
                real3 additionColor = 0;
                for (uint i = 0; i < additionalLightsCount; ++i)
                {
                    // Similar to GetMainLight, but it takes a for-loop index. This figures out the
                    // per-object light index and samples the light buffer accordingly to initialized the
                    // Light struct. If _ADDITIONAL_LIGHT_SHADOWS is defined it will also compute shadows.
                    Light light = GetAdditionalLight(i, inputData.positionWS);
                    additionColor += FallOffModify(inputData, light, _LightColor.rgb, _EdgeColor.rgb, _ShadowColor.rgb, _P1, _P2, _P3, _P4);
                }
                color = saturate(color + additionColor);
            #endif

                return real4(color,1);
            }
            ENDHLSL
        }

        // Used for rendering shadowmaps
        UsePass "Universal Render Pipeline/Lit/ShadowCaster"
    }
}