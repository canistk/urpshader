Shader "Kit/Universal Render Pipeline/Matcap Plain"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color("_Color (default = 1,1,1,1)", color) = (1,1,1,1)
        _MatCap ("MatCap (RGB)", 2D) = "white" {}

        [Header(Unity Fog)]
        [Toggle(_UnityFogEnable)] _UnityFogEnable("_UnityFogEnable (default = on)", Float) = 1

    }

    SubShader
    {
        // SubShader Tags define when and under which conditions a SubShader block or
        // a pass is executed.
        // https://docs.unity3d.com/Manual/SL-SubShaderTags.html
        Tags {
            "RenderType" = "Opaque"
            "Queue" = "Geometry" // Queue : { Background, Geometry, AlphaTest, Transparent, Overlay }
            "RenderPipeline" = "UniversalPipeline"
            // "DisableBatching" = "True"
        }
        LOD 200
        Lighting Off
		
        Pass
        {
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog
            
            // https://www.khronos.org/registry/OpenGL/extensions/NV/NV_fragment_program4.txt
            #pragma fragmentoption ARB_precision_hint_fastest


            // due to using ddx() & ddy()
            // #pragma target 3.0

            #pragma shader_feature_local _UnityFogEnable

            // The Core.hlsl file contains definitions of frequently used HLSL
            // macros and functions, and also contains #include references to other
            // HLSL files (for example, Common.hlsl, SpaceTransforms.hlsl, etc.).
            // https://github.com/Unity-Technologies/Graphics/blob/master/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

            sampler2D _MainTex;
            uniform sampler2D _MatCap;
            CBUFFER_START(UnityPerMaterial)
                float4 _MainTex_ST;
                half4 _Color;
            CBUFFER_END

            // The structure definition defines which variables it contains.
            // This example uses the Attributes structure as an input structure in
            // the vertex shader.
            struct Attributes
            {
                // The positionOS variable contains the vertex positions in object
                // space.
                float4 positionOS : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
                float3 positionWS : TEXCOORD1;
            };

            struct v2f
            {
                // The positions in this struct must have the SV_POSITION semantic.
                float4 positionCS : SV_POSITION;
                float4 uv_matcap : TEXCOORD0; // uv = xy, matcap = zw
                float4 positionWS_fog : TEXCOORD1;
            };
          
            float2 CalcMapcapUV(float3 normal)
            {
                float3 worldNormal = normalize(
                    unity_WorldToObject[0].xyz * normal.x +
                    unity_WorldToObject[1].xyz * normal.y +
                    unity_WorldToObject[2].xyz * normal.z);
                worldNormal = mul((float3x3)UNITY_MATRIX_V, worldNormal);
                return worldNormal.xy * 0.5 + 0.5; // -1~1 remap 0~1
            }


            // The vertex shader definition with properties defined in the Varyings 
            // structure. The type of the vert function must match the type (struct)
            // that it returns.
            v2f vert(Attributes IN)
            {
                // Declaring the output object (OUT) with the Varyings struct.
                v2f OUT;

                // VertexPositionInputs contains position in multiple spaces (world, view, homogeneous clip space, ndc)
                // Unity compiler will strip all unused references (say you don't use view space).
                // Therefore there is more flexibility at no additional cost with this struct.
                VertexPositionInputs vertexPositionInput = GetVertexPositionInputs(IN.positionOS.xyz);

                // The TransformObjectToHClip function transforms vertex positions
                // from object space to homogenous clip space.
                OUT.positionCS = vertexPositionInput.positionCS;
                OUT.positionWS_fog = float4(vertexPositionInput.positionWS, 0);

                // regular unity fog
#if _UnityFogEnable
                OUT.positionWS_fog.w = ComputeFogFactor(OUT.positionCS.z);
#endif

                OUT.uv_matcap = float4(IN.uv, CalcMapcapUV(IN.normal));
                return OUT;
            }

            // The fragment shader definition.            
            float4 frag(v2f IN) : SV_Target
            {
                // Defining the color variable and returning it.
                float4 col = _Color * tex2D(_MainTex, IN.uv_matcap.xy);
                float4 matcap = tex2D(_MatCap, IN.uv_matcap.zw);
                col *= matcap;

#if _UnityFogEnable
                // Mix the pixel color with fogColor. You can optionaly use MixFogColor to override the fogColor
                // with a custom one.
                col.rgb = MixFog(col.rgb, IN.positionWS_fog.w);
#endif
                return col;
            }
            ENDHLSL
        }
    }
}