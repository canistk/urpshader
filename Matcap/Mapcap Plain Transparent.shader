Shader "Kit/Universal Render Pipeline/Matcap Plain Transparent"
{
    Properties
    {
        [MainColor] _BaseColor ("BaseColor", color) = (1,1,1,1)
        [MainTexture] _BaseMap ("Texture", 2D) = "white" {}
        _MatCap ("MatCap (RGB)", 2D) = "white" {}

        [Header(Unity Fog)]
        [Toggle(_UnityFogEnable)] _UnityFogEnable("_UnityFogEnable (default = on)", Float) = 1
        
        [Header(Blending)]
        // https://docs.unity3d.com/ScriptReference/Rendering.BlendMode.html
        [Enum(UnityEngine.Rendering.BlendMode)]_SrcBlend("_SrcBlend (default = SrcAlpha)", float) = 5 // 5 = SrcAlpha
        [Enum(UnityEngine.Rendering.BlendMode)]_DstBlend("_DstBlend (default = OneMinusSrcAlpha)", float) = 10 // 10 = OneMinusSrcAlpha
        
    }

    SubShader
    {
        // SubShader Tags define when and under which conditions a SubShader block or
        // a pass is executed.
        // https://docs.unity3d.com/Manual/SL-SubShaderTags.html
        Tags {
            "RenderType"        = "Transparent"
            "Queue"             = "Transparent" // Queue : { Background, Geometry, AlphaTest, Transparent, Overlay }
            "RenderPipeline"    = "UniversalPipeline"
            //"IgnoreProjector"   = "True"
            // "DisableBatching" = "True"
        }
        LOD 300
        Blend[_SrcBlend][_DstBlend]
        // Blend One OneMinusSrcColor
		// Cull Off
		// Lighting Off
		// ZWrite Off
		
        Pass
        {
            HLSLPROGRAM
            #pragma vertex MyVertexShader
            #pragma fragment MyFragmentShader
            #pragma multi_compile_fog
            
            // https://www.khronos.org/registry/OpenGL/extensions/NV/NV_fragment_program4.txt
            #pragma fragmentoption ARB_precision_hint_fastest


            // due to using ddx() & ddy()
            #pragma target 2.0

            #pragma shader_feature_local _UnityFogEnable

            // The Core.hlsl file contains definitions of frequently used HLSL
            // macros and functions, and also contains #include references to other
            // HLSL files (for example, Common.hlsl, SpaceTransforms.hlsl, etc.).
            // https://github.com/Unity-Technologies/Graphics/blob/master/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

            TEXTURE2D(_BaseMap);        SAMPLER(sampler_BaseMap);
            TEXTURE2D(_MatCap);         SAMPLER(sampler_MatCap);

            UNITY_INSTANCING_BUFFER_START(UnityPerMaterial)
                UNITY_DEFINE_INSTANCED_PROP(half4, _BaseMap_ST);
                UNITY_DEFINE_INSTANCED_PROP(half4, _MatCap_ST);
                UNITY_DEFINE_INSTANCED_PROP(half4, _BaseColor);
            UNITY_INSTANCING_BUFFER_END(UnityPerMaterial)

            // The structure definition defines which variables it contains.
            // This example uses the Attributes structure as an input structure in
            // the vertex shader.
            struct Attributes
            {
                float4 positionOS   : POSITION;
                float2 uv           : TEXCOORD0;
                float3 normalOS       : NORMAL;
                float3 positionWS   : TEXCOORD1;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct Varyings
            {
                float4 positionCS       : SV_POSITION;
                float4 uv_matcap        : TEXCOORD0; // uv = xy, matcap = zw
                float4 positionWS_fog   : TEXCOORD1;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                UNITY_VERTEX_OUTPUT_STEREO
            };
          
            float2 CalcMapcapUV(float3 normalOS)
            {
                float3 worldNormal = normalize(
                    unity_WorldToObject[0].xyz * normalOS.x +
                    unity_WorldToObject[1].xyz * normalOS.y +
                    unity_WorldToObject[2].xyz * normalOS.z);
                worldNormal = mul((float3x3)UNITY_MATRIX_V, worldNormal);
                return worldNormal.xy * 0.5 + 0.5; // -1~1 remap 0~1
            }


            // The vertex shader definition with properties defined in the Varyings 
            // structure. The type of the vert function must match the type (struct)
            // that it returns.
            Varyings MyVertexShader(Attributes IN)
            {
                // Declaring the output object (OUT) with the Varyings struct.
                Varyings OUT;
                // https://docs.unity3d.com/Manual/SinglePassInstancing.html // Support in 2020.3
                UNITY_SETUP_INSTANCE_ID(IN);
                //UNITY_INITIALIZE_OUTPUT(Varyings, OUT); // Support in 2020.3
                // https://docs.unity3d.com/Manual/SinglePassStereoRendering.html
                // https://docs.unity3d.com/Manual/Android-SinglePassStereoRendering.html
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT); // VR support - Single-Pass Stereo Rendering for Android
                UNITY_TRANSFER_INSTANCE_ID(IN, OUT); // necessary only if you want to access instanced properties in the fragment Shader.

                // VertexPositionInputs contains position in multiple spaces (world, view, homogeneous clip space, ndc)
                // Unity compiler will strip all unused references (say you don't use view space).
                // Therefore there is more flexibility at no additional cost with this struct.
                VertexPositionInputs vertexPositionInput = GetVertexPositionInputs(IN.positionOS.xyz);

                // The TransformObjectToHClip function transforms vertex positions
                // from object space to homogenous clip space.
                OUT.positionCS = vertexPositionInput.positionCS;
                OUT.positionWS_fog = float4(vertexPositionInput.positionWS, 0);

                // regular unity fog
#if _UnityFogEnable
                OUT.positionWS_fog.w = ComputeFogFactor(OUT.positionCS.z);
#endif

                OUT.uv_matcap = float4(IN.uv, CalcMapcapUV(IN.normalOS));
                return OUT;
            }

            // The fragment shader definition.            
            float4 MyFragmentShader(Varyings IN) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID(IN); // necessary only if any instanced properties are going to be accessed in the fragment Shader.
                UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(IN); // VR support - Single-Pass Stereo Rendering for Android
                
                // Defining the color variable and returning it.
                real4 i_BaseColor           = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _BaseColor);
                real4 i_BaseMap_ST          = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _BaseMap_ST);
                real2 mainUV                = IN.uv_matcap.xy * i_BaseMap_ST.xy + i_BaseMap_ST.zw;
                real4 mainTex               = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, mainUV);

                real4 i_MatCap_ST           = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _MatCap_ST);
                real2 matcapUV              = IN.uv_matcap.zw * i_MatCap_ST.xy + i_MatCap_ST.zw;
                real4 matcap                = SAMPLE_TEXTURE2D(_MatCap, sampler_MatCap, matcapUV);

                real3 color                 = mainTex.rgb * (i_BaseColor.rgb * matcap.rgb);

#if _UnityFogEnable
                // Mix the pixel color with fogColor. You can optionaly use MixFogColor to override the fogColor
                // with a custom one.
                color.rgb = MixFog(color.rgb, IN.positionWS_fog.w);
#endif
                return real4(color, mainTex.a);
            }
            ENDHLSL
        }
    }
}