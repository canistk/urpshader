using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit
{
    public class MetaSphere : MetaShape
    {
        public float radius = 1f;

        public override MetaShapeFattenData Snapshot()
        {
            return new MetaShapeFattenData
            {
                type        = MetaShapeType.Sphere,
                position    = transform.position,
                rotation    = transform.rotation,
                color       = color,
                radius      = radius,
            };
        }
    }
}