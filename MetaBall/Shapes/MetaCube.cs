using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit
{
    public class MetaCube : MetaShape
    {
        public Vector3 size = Vector3.one;

        public override MetaShapeFattenData Snapshot()
        {
            return new MetaShapeFattenData
            {
                type        = MetaShapeType.Cube,
                position    = transform.position,
                rotation    = transform.rotation,
                color       = color,
                size        = size,
            };
        }
    }
}