using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit
{
    [ExecuteInEditMode]
    public abstract class MetaShape : MonoBehaviour
    {
        public Color color = Color.gray;

        protected virtual void OnEnable()
        {
            MetaBallRenderPass.instance.Register(this);
        }

        protected virtual void OnDisable()
        {
            MetaBallRenderPass.instance.Unregister(this);
        }

        public Vector3 position => transform.position;
        public abstract MetaShapeFattenData Snapshot();
    }

    [System.Flags]
    public enum MetaShapeType
    {
        None        = 0,
        Sphere      = 1 << 0,
        Cube        = 1 << 1,
    }

    public struct MetaShapeFattenData
    {
        public MetaShapeType    type;       // 8
        public Vector3          position;   // 8 * 3
        public Quaternion       rotation;   // 8 * 4
        public Color            color;      // 8 * 4
        public float            radius;     // 8
        public Vector3          size;       // 8 * 3

        public const int MemSize =
            sizeof(MetaShapeType) +
            sizeof(float) * 3 +
            sizeof(float) * 4 +
            sizeof(float) * 4 +
            sizeof(float) +
            sizeof(float) * 3;
    }
}