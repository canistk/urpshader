﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

namespace Kit
{
    /// <summary>
    /// <see cref="https://iquilezles.org/articles/distfunctions/"/>
    /// </summary>
    public class MetaBallRenderPass : MonoRenderPass
    {
        public override RenderPassEvent renderPassEvent => RenderPassEvent.BeforeRenderingTransparents;
        public override RenderTargetIdentifier cameraColorTarget { get; set; }

        public Material m_MetaBallMaterial = null;

        #region Singleton
        private static MetaBallRenderPass m_Instance;
        public static MetaBallRenderPass instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = FindObjectOfType<MetaBallRenderPass>();
                    if (m_Instance == null)
                    {
                        /// <see cref="Awake"/> should handle instance.
                        new GameObject(nameof(MetaBallRenderPass), typeof(MetaBallRenderPass));
                    }
                }
                return m_Instance;
            }
        }
        #endregion Singleton

        #region System
        private bool m_Destroy = false;
        private void Awake()
        {
            if (m_Instance != null && !m_Instance.m_Destroy)
            {
                // other still exist, destroy this.
                this.enabled = false;
                Destroy(this);
                return;
            }
            m_Instance = this;
        }
        private void OnDestroy()
        {
            m_Destroy = true;
        }
        private void OnApplicationQuit()
        {
            m_Destroy = true;
        }
        #endregion System

        private ComputeShader   m_MetaBallShader;
        private ComputeBuffer   m_MetaShapeBuffer;
        private List<MetaShape> m_Shapes = new List<MetaShape>();
        private bool            m_Dirty = false;
        public void Register(MetaShape shape)
        {
            m_Shapes.Add(shape);
        }

        public void Unregister(MetaShape shape)
        {
            m_Shapes.Remove(shape);
        }

        Bounds m_CameraBounds;
        
        public override void OnCameraPreCull(ScriptableRenderer renderer, in CameraData cameraData)
        {
            var cam                 = cameraData.camera.transform;
            var nearDistance        = cameraData.camera.nearClipPlane;
            var farDistance         = cameraData.camera.farClipPlane;
            var cameraAspect        = cameraData.camera.aspect;

            Vector3 nearTopLeft     = cam.position + cam.forward * nearDistance - cam.right * nearDistance  * cameraAspect + cam.up * nearDistance;
            Vector3 nearTopRight    = cam.position + cam.forward * nearDistance + cam.right * nearDistance  * cameraAspect + cam.up * nearDistance;
            Vector3 nearBottomLeft  = cam.position + cam.forward * nearDistance - cam.right * nearDistance  * cameraAspect - cam.up * nearDistance;
            Vector3 nearBottomRight = cam.position + cam.forward * nearDistance + cam.right * nearDistance  * cameraAspect - cam.up * nearDistance;
            Vector3 farTopLeft      = cam.position + cam.forward * farDistance  - cam.right * farDistance   * cameraAspect + cam.up * farDistance;
            Vector3 farTopRight     = cam.position + cam.forward * farDistance  + cam.right * farDistance   * cameraAspect + cam.up * farDistance;
            Vector3 farBottomLeft   = cam.position + cam.forward * farDistance  - cam.right * farDistance   * cameraAspect - cam.up * farDistance;
            Vector3 farBottomRight  = cam.position + cam.forward * farDistance  + cam.right * farDistance   * cameraAspect - cam.up * farDistance;

            m_CameraBounds = new Bounds(nearTopLeft, Vector3.zero);
            m_CameraBounds.Encapsulate(nearTopLeft      );
            m_CameraBounds.Encapsulate(nearTopRight     );
            m_CameraBounds.Encapsulate(nearBottomLeft   );
            m_CameraBounds.Encapsulate(nearBottomRight  );
            m_CameraBounds.Encapsulate(farTopLeft       );
            m_CameraBounds.Encapsulate(farTopRight      );
            m_CameraBounds.Encapsulate(farBottomLeft    );
            m_CameraBounds.Encapsulate(farBottomRight   );
        }

        public override void Configure(ScriptableRenderPass render, CommandBuffer cmd, RenderTextureDescriptor cameraTextureDescriptor)
        {
            m_MetaShapeBuffer?.Release();
            var pivot           = m_CameraBounds.center;
            var maxRadiusSqr    = Mathf.Max(m_CameraBounds.size.x, m_CameraBounds.size.y, m_CameraBounds.size.z);
            maxRadiusSqr *= maxRadiusSqr;

            // fatten & copy data from CPU to GPU buffer.
            if (m_Shapes.Count == 0)
                return;

            List<MetaShapeFattenData> data = new List<MetaShapeFattenData>(m_Shapes.Count);
            for (int i = 0; i < m_Shapes.Count; ++i)
            {
                var sqr = (pivot - m_Shapes[i].position).sqrMagnitude;
                if (sqr > maxRadiusSqr)
                    continue; // skip too far away.
                data.Add(m_Shapes[i].Snapshot());
            }

            if (data.Count > 0)
            {
                m_MetaShapeBuffer = new ComputeBuffer(data.Count, MetaShapeFattenData.MemSize, ComputeBufferType.Raw);
                m_MetaShapeBuffer.SetData(data);
            }
        }
        private RenderTexture m_ResultTexture;
        public override void Execute(ScriptableRenderPass render, ScriptableRenderContext context, ref RenderingData renderingData)
        {
            if (m_MetaBallMaterial == null)
                return;
            if (m_MetaShapeBuffer == null || !m_MetaShapeBuffer.IsValid())
                return;

            cameraColorTarget           = renderingData.cameraData.renderer.cameraColorTarget;
            var descriptor              = renderingData.cameraData.cameraTargetDescriptor;
            descriptor.depthBufferBits  = 0;
            var cmd                     = CommandBufferPool.Get(GetType().Name);
            var kernelIndex             = m_MetaBallShader.FindKernel("MetaBallShader");
            using (new ProfilingScope(cmd, new ProfilingSampler(GetType().Name)))
            {
                cmd.SetComputeBufferParam   (m_MetaBallShader, kernelIndex, "m_MetaShapeBuffer",    m_MetaShapeBuffer);
                cmd.SetComputeTextureParam  (m_MetaBallShader, kernelIndex, "m_Result",             m_ResultTexture);

                // cmd.DrawMesh                (RenderingUtils.fullscreenMesh, Matrix4x4.identity, m_MetaBallMaterial);

                int threadGroupsX           = Mathf.CeilToInt(m_ResultTexture.width / 8.0f);
                int threadGroupsY           = Mathf.CeilToInt(m_ResultTexture.height / 8.0f);
                cmd.DispatchCompute         (m_MetaBallShader, kernelIndex, 8, 8, 1);
            }

            context.ExecuteCommandBuffer(cmd);
            cmd.Clear();

            CommandBufferPool.Release(cmd);
        }

        public override void OnCameraCleanup(ScriptableRenderPass render, CommandBuffer cmd)
        {
            if (m_MetaShapeBuffer == null)
                return;
            m_MetaShapeBuffer.Dispose();
        }

    }
}