Shader "Kit/Universal Render Pipeline/URP/MetaBall"
{
    Properties
    {
        [MainColor] _BaseColor("Color", Color) = (0.5,0.5,0.5,1)
        [MainTexture][noscaleOffset] _BaseMap("Albedo", 2D) = "white" {}
        [noscaleOffset] _Ramp("Ramp", 2D) = "white" {}

        _Length ("Length", float) = 0.5
        _Threshold ("Threshold", Range(0, 1)) = 0.05
        
    }

    SubShader
    {
        Tags{
            "RenderType" = "Opaque"
            "Queue" = "Transparent" // Queue : { Background, Geometry, AlphaTest, Transparent, Overlay }
            "RenderPipeline" = "UniversalRenderPipeline"
            // "IgnoreProjector" = "True"
        }
        // https://docs.unity3d.com/Manual/SL-ShaderLOD.html
        LOD 300

        // ------------------------------------------------------------------
        // Forward pass. Shades GI, emission, fog and all lights in a single pass.
        // Compared to Builtin pipeline forward renderer, LWRP forward renderer will
        // render a scene with multiple lights with less drawcalls and less overdraw.
        Pass
        {
            // "Lightmode" tag must be "UniversalForward" or not be defined in order for
            // to render objects.
            Name "CustomCanisLit"
            Tags {
                "LightMode" = "UniversalForward"
            }

            HLSLPROGRAM
            #pragma target 3.0

            // -------------------------------------
            // Unity defined keywords
            #pragma multi_compile _ DIRLIGHTMAP_COMBINED
            #pragma multi_compile _ LIGHTMAP_ON
            #pragma multi_compile_fog

            //--------------------------------------
            // GPU Instancing
            #pragma multi_compile_instancing

            #pragma vertex LitPassVertex
            #pragma fragment LitPassFragment

            // Including the following two function is enought for shading with Universal Pipeline. Everything is included in them.
            // Core.hlsl will include SRP shader library, all constant buffers not related to materials (perobject, percamera, perframe).
            // It also includes matrix/space conversion functions and fog.
            // Lighting.hlsl will include the light functions/data to abstract light constants. You should use GetMainLight and GetLight functions
            // that initialize Light struct. Lighting.hlsl also include GI, Light BDRF functions. It also includes Shadows.

            // Required by all Universal Render Pipeline shaders.
            // It will include Unity built-in shader variables (except the lighting variables)
            // (https://docs.unity3d.com/Manual/SL-UnityShaderVariables.html
            // It will also include many utilitary functions. 
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            
            // Include this if you are doing a lit shader. This includes lighting shader variables,
            // lighting and shadow functions
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

            // Material shader variables are not defined in SRP or LWRP shader library.
            // This means _BaseColor, _BaseMap, _BaseMap_ST, and all variables in the Properties section of a shader
            // must be defined by the shader itself. If you define all those properties in CBUFFER named
            // UnityPerMaterial, SRP can cache the material properties between frames and reduce significantly the cost
            // of each drawcall.
            // In this case, for sinmplicity LitInput.hlsl is included. This contains the CBUFFER for the material
            // properties defined above. As one can see this is not part of the ShaderLibrary, it specific to the
            // LWRP Lit shader.
            //#include "Packages/com.unity.render-pipelines.universal/Shaders/LitInput.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/SurfaceInput.hlsl"

            struct Attributes
            {
                real4 positionOS   : POSITION;
                real3 normalOS     : NORMAL;
                real4 tangentOS    : TANGENT;
                real2 uv           : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct Varyings
            {
                real4 positionCS                : SV_POSITION;
                real4 positionWSAndFogFactor    : TEXCOORD0; // xyz: positionWS, w: vertex fog factor
                real3 normalWS                  : TEXCOORD1;
                real3 tangentWS                 : TEXCOORD2;
                real3 bitangentWS               : TEXCOORD3;
                real3 vertexLight               : TEXCOORD4;
                real2 uv                        : TEXCOORD5; // xy: UV
                UNITY_VERTEX_INPUT_INSTANCE_ID
                UNITY_VERTEX_OUTPUT_STEREO
            };

            //TEXTURE2D(_BaseMap);          SAMPLER(sampler_BaseMap);
            TEXTURE2D(_Ramp);          SAMPLER(sampler_Ramp);
            // In order to support VR & GPU Instancing
            UNITY_INSTANCING_BUFFER_START(UnityPerMaterial)
                UNITY_DEFINE_INSTANCED_PROP(half4, _BaseColor);
                UNITY_DEFINE_INSTANCED_PROP(half4, _BaseMap_ST);
                UNITY_DEFINE_INSTANCED_PROP(half, _Length);
                UNITY_DEFINE_INSTANCED_PROP(half, _Threshold);
            UNITY_INSTANCING_BUFFER_END(UnityPerMaterial)

            Varyings LitPassVertex(Attributes IN)
            {
                Varyings OUT = (Varyings)0;
                
                // https://docs.unity3d.com/Manual/SinglePassInstancing.html // Support in 2020.3
                UNITY_SETUP_INSTANCE_ID(IN);
                //UNITY_INITIALIZE_OUTPUT(Varyings, OUT); // Support in 2020.3
                // https://docs.unity3d.com/Manual/SinglePassStereoRendering.html
                // https://docs.unity3d.com/Manual/Android-SinglePassStereoRendering.html
                UNITY_TRANSFER_INSTANCE_ID(IN, OUT); // necessary only if you want to access instanced properties in the fragment Shader.
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT); // VR support - Single-Pass Stereo Rendering for Android

                VertexPositionInputs vertexInput = GetVertexPositionInputs(IN.positionOS.xyz);
                // We just use the homogeneous clip position from the vertex input
                OUT.positionCS = vertexInput.positionCS;
                // Computes fog factor per-vertex.
                real fogFactor = ComputeFogFactor(vertexInput.positionCS.z);
                OUT.positionWSAndFogFactor = real4(vertexInput.positionWS, fogFactor);

                // Here comes the flexibility of the input structs.
                // In the variants that don't have normal map defined
                // tangentWS and bitangentWS will not be referenced and
                // GetnormalInputs is only converting normal
                // from object to world space
                VertexNormalInputs normalInput = GetVertexNormalInputs(IN.normalOS, IN.tangentOS);
                OUT.normalWS = normalInput.normalWS;
                OUT.tangentWS = normalInput.tangentWS;
                OUT.bitangentWS = normalInput.bitangentWS;

                OUT.vertexLight = VertexLighting(vertexInput.positionWS, normalInput.normalWS);
                OUT.uv.xy = IN.uv; // TRANSFORM_TEX(IN.uv, _BaseMap);
                
                return OUT;
            }

            real3 Sample(real2 uv)
            {
                return SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, uv).rgb;
            }

            real4 Ramp(real dot)
            {   
                real u = dot * 0.1 + _Time.y * 0.1;
                return SAMPLE_TEXTURE2D(_Ramp, sampler_Ramp, real2(u, 0.5));
            }

            real3 SampleBox(real2 uv, real delta)
            {
                real4 offset = _BaseMap_TexelSize.xyxy * real2(-delta, delta).xxyy;
                real3 s =
                    Sample(uv + offset.xy) + Sample(uv + offset.zy) +
                    Sample(uv + offset.xw) + Sample(uv + offset.zw);
                return s * 0.25;
            }

            real4 LitPassFragment(Varyings IN) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID(IN); // necessary only if any instanced properties are going to be accessed in the fragment Shader.
                UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(IN); // VR support - Single-Pass Stereo Rendering for Android

                //real4 albedo = Sample(IN.uv);
                //real4 ramp = Ramp(d);
                //real3 cameraPosOS = TransformWorldToObject(GetCameraPositionWS());
                //real3 localViewDir = normalize(cameraPosOS - IN.positionOS);
                real2 uv = IN.positionCS.xy * 0.5 + 0.5;
                real3 rst = Sample(uv);
                return real4(rst, 1);
                real distance = length(uv - real2(0.5,0.5));
                //real3 color = (distance <= _Threshold) ? _BaseColor.rgb : real3(0,0,0);
                // real3 color = _BaseColor.rgb * smoothstep(_Threshold - 0.05, _Threshold + 0.05, distance);

                return real4(_BaseColor.rgb, 1);
            }
            ENDHLSL
        }

        // Used for rendering shadowmaps
        UsePass "Universal Render Pipeline/Lit/ShadowCaster"

        // Used for depth prepass
        // If shadows cascade are enabled we need to perform a depth prepass. 
        // We also need to use a depth prepass in some cases camera require depth texture
        // (e.g, MSAA is enabled and we can't resolve with Texture2DMS
        UsePass "Universal Render Pipeline/Lit/DepthOnly"

        UsePass "Universal Render Pipeline/Lit/DepthNormals"

    }

    // Uses a custom shader GUI to display settings. Re-use the same from Lit shader as they have the
    // same properties.
    // CustomEditor "UnityEditor.Rendering.Universal.ShaderGUI.LitShader"
}