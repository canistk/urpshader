Shader "Kit/Universal Render Pipeline/Blend Texture V2"
{
    Properties
    {
        _MainTex ("Main Texture", 2D) = "white" {}
        _AltTex ("Alt Texture", 2D) = "black" {}
        _NoiseTex ("Noise", 2D) = "white" {}
        [HDR]_EmissionColor("_EmissionColor (default = 1,1,1,1)", color) = (1,1,1,1)

        _Threshold("_Threshold", Range(0.0,1.0)) = 0
        _GrowRange("_GrowRange", Range(0.0,0.3)) = 0.1

        [Header(Unity Fog)]
        [Toggle(_UnityFogEnable)] _UnityFogEnable("_UnityFogEnable (default = on)", Float) = 1
    }

    SubShader
    {
        // https://docs.unity3d.com/Manual/SL-SubShaderTags.html
        Tags {
            "RenderType" = "Opaque"
            "Queue" = "Geometry" // Queue : { Background, Geometry, AlphaTest, Transparent, Overlay }
            "RenderPipeline" = "UniversalPipeline"
            // "DisableBatching" = "True"
        }
        LOD 100
		
        Pass
        {
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog
            #pragma target 3.0

            #pragma shader_feature_local _UnityFogEnable

            // The Core.hlsl file contains definitions of frequently used HLSL
            // macros and functions, and also contains #include references to other
            // HLSL files (for example, Common.hlsl, SpaceTransforms.hlsl, etc.).
            // https://github.com/Unity-Technologies/Graphics/blob/master/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            #include "../ShaderLibrary/KitShader.hlsl"

            sampler2D _MainTex;
            sampler2D _AltTex;
            sampler2D _NoiseTex;
            CBUFFER_START(UnityPerMaterial)
                float4 _MainTex_ST;
                float4 _AltTex_ST;
                float4 _NoiseTex_ST;
                half4 _EmissionColor;
                float _Threshold;
                float _GrowRange;
            CBUFFER_END

            struct Attributes
            {
                // The positionOS variable contains the vertex positions in object
                // space.
                float4 positionOS : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                // The positions in this struct must have the SV_POSITION semantic.
                float4 positionCS : SV_POSITION;
                float3 uv_fog : TEXCOORD0; // uv = xy, fog = z
            };            

            v2f vert(Attributes IN)
            {
                v2f OUT;
                VertexPositionInputs vertexPositionInput = GetVertexPositionInputs(IN.positionOS.xyz);
                OUT.positionCS = vertexPositionInput.positionCS;

                // regular unity fog
#if _UnityFogEnable
                OUT.uv_fog = float3(IN.uv, ComputeFogFactor(OUT.positionCS.z));
#else
                OUT.uv_fog = float3(IN.uv, 0);
#endif
                return OUT;
            }



            // The fragment shader definition.            
            float4 frag(v2f IN) : SV_Target
            {
                // calculate UV
                float2 uv1 = TRANSFORM_TEX(IN.uv_fog.xy, _MainTex);
                float2 uv2 = TRANSFORM_TEX(IN.uv_fog.xy, _AltTex);
                float2 uv3 = TRANSFORM_TEX(IN.uv_fog.xy, _NoiseTex);
                // read color from texture.
                float4 col1 = tex2D(_MainTex, uv1);
                float4 col2 = tex2D(_AltTex, uv2);
                float noise = clamp(tex2D(_NoiseTex, uv3).r, 0.001, 0.999);


                float t = _Threshold;
                float nt = 1 - t;
                float g = _GrowRange * t;
                float ng = _GrowRange * nt;

                // Solution V2
                // Known issue : Zero g flip bug.
                // float blend = remap(t + g, t - ng, 0, 1, noise);

                // Solution V3, hard edge
                //float blend = step(noise, t);

                // Solution V4
                float blend = smoothstep(noise - g, noise + ng, t);
                blend = clamp(blend, 0, 1);
                
                // float4 col = lerp(col1, col2, blend);
                float4 col = col1 * (1 - blend) + col2 * blend;



                // define range
                float min = step(noise - g, t);
                float max = step(noise + ng, t);
                
                // apply current threshold
                float f = smoothstep(min, max, t);

                // trim when it's too close to 1
                f = clamp(f * step(f, 0.999), 0, 1);

                // Apply emission
                float4 emis = lerp(float4(1, 1, 1, 1), _EmissionColor, f);
                col *= emis;

#if _UnityFogEnable
                col.rgb = MixFog(col.rgb, IN.uv_fog.z);
#endif

                return col;
            }
            ENDHLSL
        }
    }
}