Shader "Kit/Universal Render Pipeline/Blend Texture V5"
{
    Properties
    {
        _MainTex ("Main Texture", 2D) = "white" {}
        _AltTex ("Alt Texture", 2D) = "black" {}
        _MaskTex ("Mask Texture", 2D) = "black" {}
        [Space(10)]
        
        [KeywordEnum(Normal,Radial,Rhombus)]
        _Mode("Mode", Int) = 0
        _PivotX("UV Pivot X", Range(0.0,1.0)) = 0.5
        _PivotY("UV Pivot Y", Range(0.0,1.0)) = 0.5
        _Dissolve ("Dissolve", Range(0,1)) = 0.0

        [Space(10)]
        [Header(Edge)]
        _EdgeWidth("Edge width", Range(0.0,1.0)) = 0
        _EdgeSoftness("Edge Softness", Range(0.0, 1.0)) = 0.0
        _EdgeColor("Edge Color", Color) = (1,0,0,1)

    }

    SubShader
    {
        // https://docs.unity3d.com/Manual/SL-SubShaderTags.html
        Tags {
            "RenderType" = "Opaque"
            "Queue" = "Geometry" // Queue : { Background, Geometry, AlphaTest, Transparent, Overlay }
            "RenderPipeline" = "UniversalPipeline"
            // "DisableBatching" = "True"
        }
        LOD 100
		
        Pass
        {
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            // https://github.com/Unity-Technologies/Graphics/blob/master/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            #include "../ShaderLibrary/KitShader.hlsl"

            //TEXTURE2D(_BaseMap);      SAMPLER(sampler_BaseMap);
            TEXTURE2D(_MainTex);        SAMPLER(sampler_MainTex);
            TEXTURE2D(_AltTex);         SAMPLER(sampler_AltTex);
            TEXTURE2D(_MaskTex);        SAMPLER(sampler_MaskTex);
            
            UNITY_INSTANCING_BUFFER_START(UnityPerMaterial)
                UNITY_DEFINE_INSTANCED_PROP(real4,  _MainTex_ST);
                UNITY_DEFINE_INSTANCED_PROP(real4,  _AltTex_ST);
                UNITY_DEFINE_INSTANCED_PROP(real4,  _MaskTex_ST);
                UNITY_DEFINE_INSTANCED_PROP(real4,  _EdgeColor);
                UNITY_DEFINE_INSTANCED_PROP(real,   _Dissolve);
                UNITY_DEFINE_INSTANCED_PROP(real,   _EdgeWidth);
                UNITY_DEFINE_INSTANCED_PROP(real,   _EdgeSoftness);
                UNITY_DEFINE_INSTANCED_PROP(int,   _Mode);
                UNITY_DEFINE_INSTANCED_PROP(real,   _PivotX);
                UNITY_DEFINE_INSTANCED_PROP(real,   _PivotY);
                
            UNITY_INSTANCING_BUFFER_END(UnityPerMaterial)
            
            struct Attributes
            {
                // The positionOS variable contains the vertex positions in object
                // space.
                float4 positionOS : POSITION;
                float3 normalOS : NORMAL;
                float2 uv : TEXCOORD0;
                float4 color : COLOR;
            };

            struct Varyings
            {
                // The positions in this struct must have the SV_POSITION semantic.
                float4 positionCS   : SV_POSITION;
                float3 normalWS     : NORMAL;
                float2 uv           : TEXCOORD0;
                float3 positionWS   : TEXCOORD1;
                float4 color        : COLOR;
            };            

            Varyings vert(Attributes IN)
            {
                Varyings OUT;
                VertexPositionInputs vertexPositionInput = GetVertexPositionInputs(IN.positionOS.xyz);
                
                OUT.positionCS = vertexPositionInput.positionCS;
                OUT.positionWS = vertexPositionInput.positionWS;
                OUT.normalWS = TransformObjectToWorldNormal(IN.normalOS);
                OUT.color = IN.color;
                // float2 uv = TRANSFORM_TEX(IN.uv, _NoiseTex);
                OUT.uv = IN.uv;

                return OUT;
            }

            #define MY_TEXTURE(name, uv) SAMPLE_TEXTURE2D(name, sampler##name, uv.xy * name##_ST.xy + name##_ST.zw)

            // The fragment shader definition.            
            float4 frag(Varyings IN) : SV_Target
            {
                const static float epsilon = 1E-4;

                // read color from texture.
                float4 tex1 = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, TRANSFORM_TEX(IN.uv, _MainTex));
                float4 tex2 = MY_TEXTURE(_AltTex, IN.uv);
                float mask  = 1 - MY_TEXTURE(_MaskTex, IN.uv).r;
                //return float4(mask, mask, mask, 1);

                float hardEdge = _EdgeWidth / 2;
				float softEdge = hardEdge + _EdgeSoftness + epsilon;

                float2 pivot = float2(_PivotX, _PivotY);
				if (_Mode == 1) { // Radial
					float distance = length(IN.uv - pivot);
					_Dissolve = saturate(_Dissolve / max(distance, epsilon)); // epsilon to avoid div by zero
				} else if (_Mode == 2) { // Rhombus
					float2 a = IN.uv - pivot;
					float distance = abs(a.x) + abs(a.y);
					_Dissolve = saturate(_Dissolve / max(distance, epsilon)); // epsilon to avoid div by zero
				}

                // return float4(_Dissolve, _Dissolve, _Dissolve, 1);

				float dissolve = lerp(-softEdge, 1 + softEdge, _Dissolve);
                //return dissolve;

				float4 o = lerp(tex1, tex2, step(mask, dissolve));

				if (_EdgeWidth || _EdgeSoftness) { // avoid div by zero
					float e = abs(mask - dissolve);
					//float w = invLerp(hardEdge, softEdge, e);
					float w = smoothstep(hardEdge, softEdge, e);
                    // float w = smoothstep(hardEdge - softEdge, softEdge , e);
                    
					o = lerp(_EdgeColor, o,  saturate(w));
				}

                return o;
            }
            ENDHLSL
        }
    }
}