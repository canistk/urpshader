Shader "Kit/Universal Render Pipeline/Ramp Color"
{
    Properties
    {
        [Header(Gradient Color)]
        [HDR] _Color01 ("Color 1", color) = (1,0,0,1)
        [HDR] _Color02 ("Color 2", color) = (1,1,0,1)
        [HDR] _Color03 ("Color 3", color) = (0,1,0,1)
        [HDR] _Color04 ("Color 4", color) = (0,1,1,1)
        _Anchor1("Anchor 1", Range(0.0, 1.0)) = 0.0
        _Anchor2("Anchor 2", Range(0.0, 1.0)) = 0.0
        _Anchor3("Anchor 3", Range(0.0, 1.0)) = 0.0
        _Anchor4("Anchor 4", Range(0.0, 1.0)) = 0.0
    }

    // The SubShader block containing the Shader code. 
    SubShader
    {
        // SubShader Tags define when and under which conditions a SubShader block or
        // a pass is executed.
        // https://docs.unity3d.com/Manual/SL-SubShaderTags.html
        Tags {
            "RenderType" = "Opaque"
            "Queue" = "Geometry" // Queue : { Background, Geometry, AlphaTest, Transparent, Overlay }
            "RenderPipeline" = "UniversalPipeline"
            // "DisableBatching" = "True"
        }
        LOD 100
		
        Pass
        {
            HLSLPROGRAM
            #pragma vertex LitPassVertex
            #pragma fragment LitPassFragment
            
            // due to using ddx() & ddy()
            #pragma target 3.0
            
            // The Core.hlsl file contains definitions of frequently used HLSL
            // macros and functions, and also contains #include references to other
            // HLSL files (for example, Common.hlsl, SpaceTransforms.hlsl, etc.).
            // https://github.com/Unity-Technologies/Graphics/blob/master/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            #include "../ShaderLibrary/KitShader.hlsl"


            UNITY_INSTANCING_BUFFER_START(UnityPerMaterial)
                UNITY_DEFINE_INSTANCED_PROP(real4,  _Color01);
                UNITY_DEFINE_INSTANCED_PROP(real4,  _Color02);
                UNITY_DEFINE_INSTANCED_PROP(real4,  _Color03);
                UNITY_DEFINE_INSTANCED_PROP(real4,  _Color04);
                UNITY_DEFINE_INSTANCED_PROP(real,   _Anchor1);
                UNITY_DEFINE_INSTANCED_PROP(real,   _Anchor2);
                UNITY_DEFINE_INSTANCED_PROP(real,   _Anchor3);
                UNITY_DEFINE_INSTANCED_PROP(real,   _Anchor4);
            UNITY_INSTANCING_BUFFER_END(UnityPerMaterial)
            
            struct Attributes
            {
                real4 positionOS    : POSITION;
                real3 normalOS      : NORMAL;
                real2 uv            : TEXCOORD0;
                real4 color         : COLOR;
            };

            struct Varyings
            {
                real4 positionCS    : SV_POSITION;
                real4 color         : COLOR;
                real3 normalWS      : NORMAL;
                real2 uv            : TEXCOORD0; // uv = xy, matcap = zw
                real3 positionWS    : TEXCOORD1;
            };

            // assume all input range 0~1, e.g. NdotL * 0.5 + 0.5
            real4 GetGradientValue(real ndotl, real brightest, real bright, real dark, real darkest)
            {
                real4 s1 = lerp(_Color02, _Color01, saturate(invLerp(bright, brightest, ndotl)));
                real4 s2 = lerp(_Color03, _Color02, saturate(invLerp(dark, bright, ndotl)));
                real4 s3 = lerp(_Color04, _Color03, saturate(invLerp(darkest, dark, ndotl)));
                real4 s23 = lerp(s2, s3, step(ndotl, dark));
                return lerp(s1, s23, step(ndotl, bright));
            }

            Varyings LitPassVertex(Attributes IN)
            {
                Varyings OUT;
                VertexPositionInputs vertexInput = GetVertexPositionInputs(IN.positionOS.xyz);
                
                OUT.positionCS = vertexInput.positionCS;
                OUT.positionWS = vertexInput.positionWS;
                OUT.normalWS = TransformObjectToWorldNormal(IN.normalOS);
                OUT.color = IN.color;
                OUT.uv  = IN.uv;
                return OUT;
            }

            // The fragment shader definition.         
            real4 LitPassFragment(Varyings IN) : SV_Target
            {
                Light light     = GetMainLight();
                real NdotL      = dot(IN.normalWS, light.direction);
                real NdotL01    = NdotL * 0.5 + 0.5;

                return GetGradientValue(NdotL01, _Anchor1, _Anchor2, _Anchor3, _Anchor4);
                return NdotL01;
            }
            ENDHLSL
        }
    }
}