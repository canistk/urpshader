Shader "Kit/Universal Render Pipeline/Blend Texture V1"
{
    Properties
    {
        _MainTex ("Main Texture", 2D) = "white" {}
        _AltTex ("Alt Texture", 2D) = "black" {}
        _NoiseTex ("Noise", 2D) = "white" {}
        [HDR]_EmissionColor("_EmissionColor (default = 1,1,1,1)", color) = (1,1,1,1)

        _Threshold("_Threshold", Range(0.0,1.0)) = 0
        _GrowRange("_GrowRange", Range(0.0,0.3)) = 0.1

        [Header(Unity Fog)]
        [Toggle(_UnityFogEnable)] _UnityFogEnable("_UnityFogEnable (default = on)", Float) = 1
    }

    // The SubShader block containing the Shader code. 
    SubShader
    {
        // SubShader Tags define when and under which conditions a SubShader block or
        // a pass is executed.
        // https://docs.unity3d.com/Manual/SL-SubShaderTags.html
        Tags {
            "RenderType" = "Opaque"
            "Queue" = "Geometry" // Queue : { Background, Geometry, AlphaTest, Transparent, Overlay }
            "RenderPipeline" = "UniversalPipeline"
            // "DisableBatching" = "True"
        }
        LOD 100
		
        Pass
        {
            // The HLSL code block. Unity SRP uses the HLSL language.
            HLSLPROGRAM
            // This line defines the name of the vertex shader. 
            #pragma vertex vert
            // This line defines the name of the fragment shader. 
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            // due to using ddx() & ddy()
            #pragma target 3.0

            #pragma shader_feature_local _UnityFogEnable

            // The Core.hlsl file contains definitions of frequently used HLSL
            // macros and functions, and also contains #include references to other
            // HLSL files (for example, Common.hlsl, SpaceTransforms.hlsl, etc.).
            // https://github.com/Unity-Technologies/Graphics/blob/master/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            #include "../ShaderLibrary/KitShader.hlsl"

            sampler2D _MainTex;
            sampler2D _AltTex;
            sampler2D _NoiseTex;
            CBUFFER_START(UnityPerMaterial)
                float4 _MainTex_ST;
                float4 _AltTex_ST;
                float4 _NoiseTex_ST;
                half4 _EmissionColor;
                float _Threshold;
                float _GrowRange;
            CBUFFER_END

            // The structure definition defines which variables it contains.
            // This example uses the Attributes structure as an input structure in
            // the vertex shader.
            struct Attributes
            {
                // The positionOS variable contains the vertex positions in object
                // space.
                float4 positionOS : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                // The positions in this struct must have the SV_POSITION semantic.
                float4 positionCS : SV_POSITION;
                float3 uv_fog : TEXCOORD0; // uv = xy, fog = z
            };            

            // The vertex shader definition with properties defined in the Varyings 
            // structure. The type of the vert function must match the type (struct)
            // that it returns.
            v2f vert(Attributes IN)
            {
                // Declaring the output object (OUT) with the Varyings struct.
                v2f OUT;

                // VertexPositionInputs contains position in multiple spaces (world, view, homogeneous clip space, ndc)
                // Unity compiler will strip all unused references (say you don't use view space).
                // Therefore there is more flexibility at no additional cost with this struct.
                VertexPositionInputs vertexPositionInput = GetVertexPositionInputs(IN.positionOS.xyz);

                // The TransformObjectToHClip function transforms vertex positions
                // from object space to homogenous clip space.
                OUT.positionCS = vertexPositionInput.positionCS;

                // regular unity fog
#if _UnityFogEnable
                OUT.uv_fog = float3(IN.uv, ComputeFogFactor(OUT.positionCS.z));
#else
                OUT.uv_fog = float3(IN.uv, 0);
#endif
                return OUT;
            }



            // The fragment shader definition.            
            float4 frag(v2f IN) : SV_Target
            {
                // calculate UV
                float2 uv1 = TRANSFORM_TEX(IN.uv_fog.xy, _MainTex);
                float2 uv2 = TRANSFORM_TEX(IN.uv_fog.xy, _AltTex);
                float2 uv3 = TRANSFORM_TEX(IN.uv_fog.xy, _NoiseTex);
                // read color from texture.
                float4 col1 = tex2D(_MainTex, uv1);
                float4 col2 = tex2D(_AltTex, uv2);
                float noise = clamp(tex2D(_NoiseTex, uv3).r, 0.001, 0.999);

                //noise = remap(_Threshold - noise, _Threshold - noise + _GrowRange, 0, 1, noise);
                //return float4(0, noise, 0, 1);
                float4 emis = float4(1, 1, 1, 1);
                float pt = 0;
                if (noise >= _Threshold)
                {
                    pt = 0;
                }
                else if (noise - (_GrowRange * _Threshold) < _Threshold - _GrowRange)
                {
                    pt = 1;
                }
                else // With in _GrowRange
                {
                    pt = smoothstep(_Threshold, _Threshold - _GrowRange, noise);
                    emis = _EmissionColor * (1 - pt);
                }
                pt = clamp(pt, 0, 1);

                // float4 col = lerp(col1, col2, pt);
                float4 col = col1 * (1 - pt) + col2 * pt;
                col *= emis;


#if _UnityFogEnable
                // Mix the pixel color with fogColor. You can optionaly use MixFogColor to override the fogColor
                // with a custom one.
                col.rgb = MixFog(col.rgb, IN.uv_fog.z);
#endif

                return col;
            }
            ENDHLSL
        }
    }
}