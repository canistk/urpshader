Shader "Kit/Universal Render Pipeline/PlotGraph"
{
    Properties
    {
        [MainTexture][noscaleOffset] _BaseMap("Albedo", 2D) = "white" {}
		_LineWidth("LineWidth", range(1, 50)) = 4
		_Param00("Param 00", Vector) = (0,0,0,0)
        _Param01("Param 01", Vector) = (0,0,0,0)
    }

    SubShader
    {
        Tags{
            "RenderType" = "Opaque"
            "Queue" = "Geometry" // Queue : { Background, Geometry, AlphaTest, Transparent, Overlay }
            "RenderPipeline" = "UniversalRenderPipeline"
            // "IgnoreProjector" = "True"
        }
        // https://docs.unity3d.com/Manual/SL-ShaderLOD.html
        LOD 100
        Cull Off

        Pass
        {
            // "Lightmode" tag must be "UniversalForward" or not be defined in order for
            // to render objects.
            Name "First Pass"
            Tags {
                "RenderType"    = "Opaque"
                "LightMode"     = "UniversalForward"
            }

            HLSLPROGRAM
            #pragma target 3.0
            #pragma vertex LitPassVertex
            #pragma fragment LitPassFragment

            // Optional Parameters
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            struct Attributes
            {
                real4 positionOS    : POSITION;
                real2 uv            : TEXCOORD0;
            };

            struct Varyings
            {
                real4 positionCS    : SV_POSITION;
                real3 positionWS    : TEXCOORD0;
                real2 uv            : TEXCOORD1;
            };

            TEXTURE2D(_BaseMap);          SAMPLER(sampler_BaseMap);
            // In order to support VR & GPU Instancing
            UNITY_INSTANCING_BUFFER_START(UnityPerMaterial)
                UNITY_DEFINE_INSTANCED_PROP(real4, _BaseMap_ST);
                UNITY_DEFINE_INSTANCED_PROP(real4, _Param00);
                UNITY_DEFINE_INSTANCED_PROP(real4, _Param01);
                UNITY_DEFINE_INSTANCED_PROP(real, _LineWidth);
            UNITY_INSTANCING_BUFFER_END(UnityPerMaterial)
            
            Varyings LitPassVertex(Attributes IN)
            {
                Varyings OUT = (Varyings)0;
                VertexPositionInputs vertexInput = GetVertexPositionInputs(IN.positionOS.xyz);
                // VertexNormalInputs normalInput = GetVertexNormalInputs(IN.normalOS, IN.tangentOS);
                OUT.positionCS = vertexInput.positionCS;
                OUT.positionWS = vertexInput.positionWS;
                // OUT.uv  = TRANSFORM_TEX(IN.uv, _BaseMap);
                OUT.uv = IN.uv * _BaseMap_ST.xy + _BaseMap_ST.zw;
                return OUT;
            }

            float Formula01(float x, float4 param1, float4 param2)
            {
                // Base formula
                float t = abs(frac(x) * 2 - 1);
                //return t;
                

                float v0 = Smoothstep01(t);
                return v0;

                // Smoothstep's formula
                float v1 = t * t * (3.0 - (2.0 * t));
                return v1;
            }

            float Formula02(float x, float4 param1, float4 param2)
            {
                return pow(abs(x), param1.x);
                return -param1.x * x * x * x + param1.y * x;

                return -2 * x * x * x + 4 * x;
                return (x - 1) / (x * x - 9);
                return x * x; // U sharp
                return x * x + 2; // x * x shift up 2
                return (x + 2) * (x + 2); // x * x shift left 2

            }

            float4 LitPassFragment(Varyings IN) : SV_Target
            {
                real i_LineWidth    = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _LineWidth);
                real4 i_Param00     = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _Param00);
                real4 i_Param01     = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _Param01);

                float4 tex          = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, IN.uv);
                float2 graphCoord   = (IN.uv - 0.5) * 10; // 0~1, remap to -5 ~ 5, since the grid size are 5
                
                float w = i_LineWidth * ddy(IN.uv.y);   // define 1-pixel in Y-axis via ddy()

                float value01 = Formula01(graphCoord.x, i_Param00, i_Param01);       // math formula convert x to y.
                value01 = w / abs(graphCoord.y - value01);  // define "value" : based on each pixel's distance to "value" on texture.

                float value02 = Formula02(graphCoord.x, i_Param00, i_Param01);       // math formula convert x to y.
                value02 = w / abs(graphCoord.y - value02);  // define "value" : based on each pixel's distance to "value" on texture.


                float4 lineColor = saturate(lerp(float4(1,0,0,1), float4(0,1,0,1), value02 - value01));
                return lerp(tex, lineColor, saturate(value01 + value02));
                
            }
            ENDHLSL
        }

    }
}