
Shader "Kit/Universal Render Pipeline/MultiPass"
{
    Properties
    {
        _Tex01 ("Texture", 2D) = "white" {}
        _Tex02 ("Texture", 2D) = "white" {}
        _Tex03 ("Texture", 2D) = "white" {}
        _Tex04 ("Texture", 2D) = "white" {}
        _Color01("_Color (default = 1,1,1,1)", color) = (1,1,1,1)
        _Color02("_Color (default = 1,1,1,1)", color) = (1,1,1,1)
        _Color03("_Color (default = 1,1,1,1)", color) = (1,1,1,1)
        _Color04("_Color (default = 1,1,1,1)", color) = (1,1,1,1)
    }

    // The SubShader block containing the Shader code. 
    SubShader
    {
        // SubShader Tags define when and under which conditions a SubShader block or
        // a pass is executed.
        // https://docs.unity3d.com/Manual/SL-SubShaderTags.html
        Tags {
            "RenderQueue"       = "AlphaTest"
            "RenderType"        = "TransparentCutout"
            "Queue"             = "AlphaTest" // Queue : { Background, Geometry, AlphaTest, Transparent, Overlay }
            "RenderPipeline"    = "UniversalPipeline"
            "IgnoreProjector"   = "True"
            "DisableBatching"   = "True"
        }
        LOD 300
		
        Pass
        {
            Tags
            {
                "LightMode" = "Opaques-2"
            }
            Cull Back
            ZWrite On
            ZTest LEqual
            Blend SrcAlpha OneMinusSrcAlpha
            AlphaToMask On
            AlphaTest Less 0.5
            // ColorMask 0

            HLSLPROGRAM
            #pragma vertex UnlitVertexShader
            #pragma fragment UnlitFragmentShader
            #pragma target 3.0

            #include_with_pragmas "MultiPass.hlsl"
            real4 UnlitFragmentShader(Varyings IN) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID(IN);
                UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(IN);
                
                // Defining the color variable and returning it.
                real4 col = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _Color01);
                col *= MY_TEXTURE_SAMPLE(_Tex01, IN.uv);
                clip(col.a);
                return col;
            }
            ENDHLSL
        }
        
        Pass
        {
            Tags
            {
                "LightMode" = "Opaques-1"
            }
            Cull Back
            ZWrite On
            ZTest LEqual
            Blend SrcAlpha OneMinusSrcAlpha
            AlphaToMask On
            AlphaTest Less 0.5
            // ColorMask 0

            HLSLPROGRAM
            #pragma vertex UnlitVertexShader
            #pragma fragment UnlitFragmentShader
            #pragma target 3.0

            #include_with_pragmas "MultiPass.hlsl"
            real4 UnlitFragmentShader(Varyings IN) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID(IN);
                UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(IN);
                
                // Defining the color variable and returning it.
                real4 col = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _Color02);
                col *= MY_TEXTURE_SAMPLE(_Tex02, IN.uv);
                clip(col.a);
                return col;
            }
            ENDHLSL
        }

        Pass
        {
            Tags
            {
                "LightMode" = "Transparent+1"
            }
            Cull Back
            ZWrite On
            ZTest LEqual
            Blend SrcAlpha OneMinusSrcAlpha
            AlphaToMask On
            AlphaTest Less 0.5
            // ColorMask 0

            HLSLPROGRAM
            #pragma vertex UnlitVertexShader
            #pragma fragment UnlitFragmentShader
            #pragma target 3.0

            #include_with_pragmas "MultiPass.hlsl"
            real4 UnlitFragmentShader(Varyings IN) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID(IN);
                UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(IN);
                
                // Defining the color variable and returning it.
                real4 col = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _Color03);
                col *= MY_TEXTURE_SAMPLE(_Tex03, IN.uv);
                clip(col.a);
                return col;
            }
            ENDHLSL
        }

        Pass
        {
            Tags
            {
                "LightMode" = "Transparent+2"
            }
            Cull Back
            ZWrite On
            ZTest LEqual
            Blend SrcAlpha OneMinusSrcAlpha
            AlphaToMask On
            AlphaTest Less 0.5
            // ColorMask 0

            HLSLPROGRAM
            #pragma vertex UnlitVertexShader
            #pragma fragment UnlitFragmentShader
            #pragma target 3.0

            #include_with_pragmas "MultiPass.hlsl"
            real4 UnlitFragmentShader(Varyings IN) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID(IN);
                UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(IN);
                
                // Defining the color variable and returning it.
                real4 col = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _Color04);
                col *= MY_TEXTURE_SAMPLE(_Tex04, IN.uv);
                clip(col.a);
                return col;
            }
            ENDHLSL
        }
    }
}