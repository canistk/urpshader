using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
namespace Kit
{
    public class MultiRenderPassFeature : ScriptableRendererFeature
    {
        class DrawPass : ScriptableRenderPass
        {
            public List<ShaderTagId> m_ShaderTagIds;
            public RenderQueueRange renderQueueRange;
            public bool m_IsOpaque;
            public DrawPass(bool isOpaque, ShaderTagId[] shaderTagIds_, RenderQueueRange renderQueueRange_)
            {
                m_IsOpaque = isOpaque;
                m_ShaderTagIds = new List<ShaderTagId>(shaderTagIds_);
                renderQueueRange = renderQueueRange_;
            }

            public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
            {
                var sortFlags = m_IsOpaque ?
                    renderingData.cameraData.defaultOpaqueSortFlags :
                    SortingCriteria.CommonTransparent;
                // var sort = new SortingSettings(renderingData.cameraData.camera);
                var drawingSetting = CreateDrawingSettings(m_ShaderTagIds, ref renderingData, sortFlags);
                var filteringSettings = new FilteringSettings(renderQueueRange);
                context.DrawRenderers(renderingData.cullResults, ref drawingSetting, ref filteringSettings);
            }
        }

        DrawPass
            beforeOpaques, afterOpaques,
            beforeTransparent, afterTransparent;

        /// <inheritdoc/>
        public override void Create()
        {
            // Debug.Log("Created Multi Pass");
            beforeOpaques = new DrawPass(true, new[]{
                new ShaderTagId("Opaques-4"),
                new ShaderTagId("Opaques-3"),
                new ShaderTagId("Opaques-2"),
                new ShaderTagId("Opaques-1"),
            }, RenderQueueRange.opaque);
            beforeOpaques.renderPassEvent = RenderPassEvent.BeforeRenderingOpaques;
            
            afterOpaques = new DrawPass(true, new[]{
                new ShaderTagId("Opaques+1"),
                new ShaderTagId("Opaques+2"),
                new ShaderTagId("Opaques+3"),
                new ShaderTagId("Opaques+4"),
            }, RenderQueueRange.opaque);
            afterOpaques.renderPassEvent = RenderPassEvent.AfterRenderingOpaques;

            beforeTransparent = new DrawPass(false, new[]{
                new ShaderTagId("Transparent-4"),
                new ShaderTagId("Transparent-3"),
                new ShaderTagId("Transparent-2"),
                new ShaderTagId("Transparent-1"),
            }, RenderQueueRange.transparent);
            beforeTransparent.renderPassEvent = RenderPassEvent.BeforeRenderingTransparents;

            afterTransparent = new DrawPass(false, new[]{
                new ShaderTagId("Transparent+1"),
                new ShaderTagId("Transparent+2"),
                new ShaderTagId("Transparent+3"),
                new ShaderTagId("Transparent+4"),
            }, RenderQueueRange.transparent);
            afterTransparent.renderPassEvent = RenderPassEvent.AfterRenderingTransparents;
        }

        public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
        {
            // Debug.Log("AddRenderPasses");
            renderer.EnqueuePass(beforeOpaques);
            renderer.EnqueuePass(afterOpaques);

            renderer.EnqueuePass(beforeTransparent);
            renderer.EnqueuePass(afterTransparent);
        }
    }
}