#ifndef KIT_MULTIPASS
#define KIT_MULTIPASS

//------------------------------------------------

#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
#define MY_TEXTURE_SAMPLE(tex, uv) SAMPLE_TEXTURE2D(tex, sampler##tex, uv * tex##_ST.xy + tex##_ST.zw);

TEXTURE2D(_Tex01);         SAMPLER(sampler_Tex01);
TEXTURE2D(_Tex02);         SAMPLER(sampler_Tex02);
TEXTURE2D(_Tex03);         SAMPLER(sampler_Tex03);
TEXTURE2D(_Tex04);         SAMPLER(sampler_Tex04);
UNITY_INSTANCING_BUFFER_START(UnityPerMaterial)
    UNITY_DEFINE_INSTANCED_PROP(half4, _Tex01_ST);
    UNITY_DEFINE_INSTANCED_PROP(half4, _Tex02_ST);
    UNITY_DEFINE_INSTANCED_PROP(half4, _Tex03_ST);
    UNITY_DEFINE_INSTANCED_PROP(half4, _Tex04_ST);

    UNITY_DEFINE_INSTANCED_PROP(half4, _Color01);
    UNITY_DEFINE_INSTANCED_PROP(half4, _Color02);
    UNITY_DEFINE_INSTANCED_PROP(half4, _Color03);
    UNITY_DEFINE_INSTANCED_PROP(half4, _Color04);
UNITY_INSTANCING_BUFFER_END(UnityPerMaterial)

struct Attributes
{
    real4   positionOS  : POSITION;
    real2   uv          : TEXCOORD0;
    UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct Varyings
{
    real4   positionCS  : SV_POSITION;
    real2   uv          : TEXCOORD0;
    UNITY_VERTEX_INPUT_INSTANCE_ID
    UNITY_VERTEX_OUTPUT_STEREO
};

Varyings UnlitVertexShader(Attributes IN)
{
    // Declaring the output object (OUT) with the Varyings struct.
    Varyings OUT;

    UNITY_SETUP_INSTANCE_ID(IN);
    UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT); // VR support - Single-Pass Stereo Rendering for Android
    UNITY_TRANSFER_INSTANCE_ID(IN, OUT); // necessary only if you want to access instanced properties in the fragment Shader.
    VertexPositionInputs vertexPositionInput    = GetVertexPositionInputs(IN.positionOS.xyz);
    //VertexNormalInputs normalInput              = GetVertexNormalInputs(IN.normalOS, IN.tangentOS);
    OUT.positionCS  = vertexPositionInput.positionCS;
    OUT.uv          = IN.uv;

    return OUT;
}

// The fragment shader definition.            

//-----------------------------------------------
#endif