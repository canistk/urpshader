using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit
{
    public class ObjectLayerController : MonoBehaviour
    {
        [SerializeField] string m_Enter = "FakeHole";

        [SerializeField] string m_Outside;
        [SerializeField] int m_OutsideRenderQueue = 3000;

        [SerializeField] string m_Inside;
        [SerializeField] int m_InsideRenderQueue = 1997;
        private Renderer m_Renderer;

        private void Awake()
        {
            m_Renderer = GetComponent<Renderer>();
        }

        private void OnDisable()
        {
            ResetLayer();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag(m_Enter))
            {
                gameObject.layer = LayerMask.NameToLayer(m_Inside);
                foreach(var mat in m_Renderer.materials)
                {
                    mat.renderQueue = m_InsideRenderQueue;
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.CompareTag(m_Enter))
            {
                ResetLayer();
            }
        }

        private void ResetLayer()
        {
            gameObject.layer = LayerMask.NameToLayer(m_Outside);
            foreach (var mat in m_Renderer.materials)
            {
                mat.renderQueue = m_OutsideRenderQueue;
            }
        }
    }
}