using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit
{
    /// <summary>
    /// Assume put this script on "Trigger"
    /// control the enter collision,
    /// when it's matching the giving "Tag"
    /// The target will able to ignore & get inside the preset collider.
    /// </summary>
    [RequireComponent(typeof(Collider))]
    public class DynamicIgnoreCollider : MonoBehaviour
    {
        [SerializeField] string m_Tag = "Player";
        public List<Collider> m_IgnoreColliders = new List<Collider>(10);

        [Header("Override target's")]
        [SerializeField] int m_OutsideRenderQueue = 3000;
        [SerializeField] int m_InsideRenderQueue = 1997;

        private HashSet<Collider> m_Overlap = new HashSet<Collider>();

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag(m_Tag))
            {
                m_Overlap.Add(other);
                SetRenderQueue(other.gameObject, m_InsideRenderQueue);
                IgnoreCollision(other, m_IgnoreColliders, true);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.CompareTag(m_Tag) &&
                m_Overlap.Remove(other))
            {
                SetRenderQueue(other.gameObject, m_OutsideRenderQueue);
                IgnoreCollision(other, m_IgnoreColliders, false);
            }
        }

        private void OnDisable()
        {
            foreach (var other in m_Overlap)
            {
                if (other != null)
                {
                    if (other.gameObject)
                        SetRenderQueue(other.gameObject, m_OutsideRenderQueue);
                    IgnoreCollision(other, m_IgnoreColliders, false);
                }
            }
            m_Overlap.Clear();
        }

        private void SetRenderQueue(GameObject go, int renderQueue)
        {
            Renderer[] rs = go.GetComponentsInChildren<Renderer>();
            foreach (var r in rs)
                foreach (var m in r.materials)
                    m.renderQueue = renderQueue;
        }

        private void IgnoreCollision(Collider target, IList<Collider> others, bool ignore)
        {
            foreach (var collider in others)
                Physics.IgnoreCollision(target, collider, ignore);            
        }
    }
}