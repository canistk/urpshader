Shader "Kit/Universal Render Pipeline/DepthMask/Mask" {
	SubShader {
		// Render the mask after regular geometry, but before masked geometry and
		// transparent things.
 
		Tags {
			"RenderType" = "Opaque"
			"Queue" = "Geometry-1"
			// "RenderPipeline" = "UniversalPipeline"
		}
 
		// Don't draw in the RGBA channels; just the depth buffer
		Lighting Off
		ColorMask 0
		ZWrite On
		ZTest Always
 
		// Do nothing specific in the pass:
 
		Pass {}
	}
}