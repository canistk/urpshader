﻿/*
	SetRenderQueue.cs
 
	Sets the RenderQueue of an object's materials on Awake. This will instance
	the materials, so the script won't interfere with other renderers that
	reference the same materials.
*/

using UnityEngine;
[RequireComponent(typeof(Renderer))]
public class SetRenderQueue : MonoBehaviour
{
	[SerializeField] Renderer m_Renderer = null;
	[SerializeField] protected int[] m_queues = new int[] { 3000 };

    private void Reset()
    {
		m_Renderer = GetComponent<Renderer>();
	}

    protected void Awake()
	{
		if (m_Renderer == null)
			m_Renderer = GetComponent<Renderer>();
		Material[] materials = m_Renderer.materials;
		for (int i = 0; i < materials.Length && i < m_queues.Length; ++i)
		{
			materials[i].renderQueue = m_queues[i];
		}
	}
}