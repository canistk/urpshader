// https://catlikecoding.com/unity/tutorials/advanced-rendering/tessellation/
// https://forum.unity.com/threads/how-to-dynamically-tessellate-a-mesh.597469/
// https://www.patreon.com/posts/45320078

Shader "Kit/Universal Render Pipeline/Subsurface Scattering"
{
    Properties
    {
        _MainTex("Main Texture", 2D) = "white" {}
        _Color("Color (default = 1,1,1,1)", color) = (1,1,1,1)
        [Space(20)]
        [Header(SSS Light)]
        [Toggle(_SSSEnable)] _SSSEnable ("Subsurface Scattering", Float) = 1
        _Color_LightDiffuse  ("Light Diffuse", color) = (1,1,1,1)
        _Color_DiffuseAlbedo ("Diffuse Albedo", color) = (1,1,1,1)

        // translucency that is always present, both front and back.
        _LightAmbient("Light Ambient", 2D) = "white" {}
        // power value for direct translucency
        _LightPower("Light Power", Range(0.01,1)) = 1
        _LightDistortion("Light Distortion, Subsurface distortion, Breaks continuity, view-dependent, allows for more organic, Fresnel-like", Range(0,1)) = 1
        
        // Local thickness map, used for both direct and indirect translucency
        _LightThickness("Light Thickness, ", 2D) = "white" {}

        // Direct/Back translucency, View-oriented, Should be defined per-light to control the central point.
        _LightScale("Light Scale", Float) = 1
        _LightAttenuation("Light Attenuation", Float) = 1


        [Space(20)]
        [Header(Tessellation)]
        // [Toggle(_TessellationEnable)] _TessellationEnable ("Tessellation Enable", Float) = 1
        [KeywordEnum(fractional_odd,fractional_even,pow2,integer)]
        _Partitioning("Partitioning (default = integer)", Float) = 3
        _Tess("Tessellation", Range(1, 32)) = 20
        _MinTessDistance("Min Tess Distance", Range(0.01, 32)) = 3
        _MaxTessDistance("Max Tess Distance", Range(0.01, 32)) = 20
        
        [Space(10)]
        [Header(Unity Fog)]
        [Toggle(_UnityFogEnable)] _UnityFogEnable("_UnityFogEnable (default = on)", Float) = 1
    }

    // The SubShader block containing the Shader code. 
    SubShader
    {
        // SubShader Tags define when and under which conditions a SubShader block or
        // a pass is executed.
        // https://docs.unity3d.com/Manual/SL-SubShaderTags.html
        Tags {
            "RenderType" = "Opaque"
            "Queue" = "Geometry" // Queue : { Background, Geometry, AlphaTest, Transparent, Overlay }
            "RenderPipeline" = "UniversalRenderPipeline"
            
            // "DisableBatching" = "True"
        }
        LOD 100
		
        Pass
        {
            Tags {
                "LightMode" = "UniversalForward"
            }
            HLSLPROGRAM

            #if defined(SHADER_API_D3D11) || defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE) || defined(SHADER_API_VULKAN) || defined(SHADER_API_METAL) || defined(SHADER_API_PSSL)
            #define UNITY_CAN_COMPILE_TESSELLATION 1
            #   define UNITY_domain                 domain
            #   define UNITY_partitioning           partitioning
            #   define UNITY_outputtopology         outputtopology
            #   define UNITY_patchconstantfunc      patchconstantfunc
            #   define UNITY_outputcontrolpoints    outputcontrolpoints
            #endif
            // This line defines the name of the vertex shader. 
            #pragma vertex TessellationVertexProgram
            // This line defines the name of the fragment shader. 
            #pragma fragment frag
            // This line defines the name of the hull shader. 
            #pragma hull hull
            // This line defines the name of the domain shader. 
            #pragma domain domain

            #pragma multi_compile_fog

            
            // due to using Tessellation
            // https://docs.unity3d.com/2019.3/Documentation/Manual/SL-ShaderCompileTargets.html
            #pragma require tessellation
            #pragma target 4.6

            #pragma shader_feature_local _UnityFogEnable
            #pragma shader_feature_local _SSSEnable
            #pragma shader_feature_local multi_compile _TessellationEnable
            #pragma multi_compile _PARTITIONING_FRACTIONAL_ODD _PARTITIONING_FRACTIONAL_EVEN _PARTITIONING_POW2 _PARTITIONING_INTEGER

            // The Core.hlsl file contains definitions of frequently used HLSL
            // macros and functions, and also contains #include references to other
            // HLSL files (for example, Common.hlsl, SpaceTransforms.hlsl, etc.).
            // https://github.com/Unity-Technologies/Graphics/blob/master/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl
            // https://github.com/Unity-Technologies/Graphics/tree/master/com.unity.render-pipelines.core/ShaderLibrary
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Input.hlsl"

            // #include "AutoLight.cginc"
            CBUFFER_START(UnityPerMaterial)
                sampler2D _MainTex;
                float4 _MainTex_ST;
                half4 _Color;

                // Tessellation
                float _Tess;
                float _MinTessDistance;
                float _MaxTessDistance;

                // SSS
                float _LightDistortion;
                float _LightAttenuation;
                float _LightPower;
                float _LightScale;
                sampler2D _LightAmbient;
                float4 _LightAmbient_ST;
                sampler2D _LightThickness;
                float4 _LightThickness_ST;
                half4 _Color_DiffuseAlbedo;
                half4 _Color_LightDiffuse;
            CBUFFER_END

            // Pre Tessellation - Extra vertex struct
            struct ControlPoint
            {
                float4 vertex : INTERNALTESSPOS;
                float4 color : COLOR;
                float3 normal : NORMAL;
                float2 uv : TEXCOORD0;
                float2 lightmapuv : TEXCOORD1;
            };
            // tessellation data
            struct TessellationFactors
            {
                float edge[3] : SV_TessFactor;
                float inside : SV_InsideTessFactor;
            };

            // The structure definition defines which variables it contains.
            // This example uses the Attributes structure as an input structure in
            // the vertex shader.
            struct Attributes
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 color : COLOR;
                float2 uv : TEXCOORD0;
                float2 lightmapuv : TEXCOORD1;
            };

            struct Varyings
            {
                // The positions in this struct must have the SV_POSITION semantic.
                float4 positionCS : SV_POSITION;
                float4 color : COLOR;
                float3 normal : NORMAL;
                float3 normalWS : VAR_NORMAL;
                float3 uv_fog : TEXCOORD0; // uv = xy, fog = z
                float3 positionWS : TEXCOORD1;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            // Step 1) prepare Vertices data to Hull program
            // Pre tesselation vertex program
            ControlPoint TessellationVertexProgram(Attributes v)
            {
                ControlPoint p;
                p.vertex = v.vertex;
                p.normal = v.normal;
                p.color = v.color;
                p.uv = v.uv;
                p.lightmapuv = v.lightmapuv;
                return p;
            }

            // Step 2) Triangle Indices
            // info so the GPU knows what to do (triangles) and how to set it up , clockwise, fractional division
            // hull takes the original vertices and outputs more
            [UNITY_domain("tri")]
            [UNITY_outputcontrolpoints(3)]
            [UNITY_outputtopology("triangle_cw")]
#if _PARTITIONING_FRACTIONAL_ODD 
            [UNITY_partitioning("fractional_odd")]
#endif
#if _PARTITIONING_FRACTIONAL_EVEN 
            [UNITY_partitioning("fractional_even")]
#endif
#if _PARTITIONING_POW2
            [UNITY_partitioning("pow2")]
#endif
#if _PARTITIONING_INTEGER
            [UNITY_partitioning("integer")]
#endif
            [UNITY_patchconstantfunc("patchConstantFunction")] // send data to here
            ControlPoint hull(InputPatch<ControlPoint, 3> patch, uint id : SV_OutputControlPointID)
            {
                return patch[id];
            }

            // Step 3.1) optimization,
            // fade tessellation at a distance
            float CalcDistanceTessFactor(float4 vertex, float minDist, float maxDist, float tess)
            {
                float3 worldPosition = TransformObjectToWorld(vertex.xyz);
                float dist = distance(worldPosition, _WorldSpaceCameraPos);
                // Calculate the factor we need to apply tessellation 0.01 ~ 1;
                float f = clamp(1.0 - ((dist - minDist) / (maxDist - minDist)), 0.01, 1.0) * tess;
                return f;
            }

            // Step 3)
            // Tessellation, receive info from Hull, and patching data.
            TessellationFactors patchConstantFunction(InputPatch<ControlPoint, 3> patch)
            {
                // values for distance fading the tessellation
                // since wrong order will calculate in flipped result.
                // ensure the min/max values.
                float minDist = min(_MinTessDistance, _MaxTessDistance);
                float maxDist = max(_MinTessDistance, _MaxTessDistance);

                TessellationFactors f;

                float edge0 = CalcDistanceTessFactor(patch[0].vertex, minDist, maxDist, _Tess);
                float edge1 = CalcDistanceTessFactor(patch[1].vertex, minDist, maxDist, _Tess);
                float edge2 = CalcDistanceTessFactor(patch[2].vertex, minDist, maxDist, _Tess);

                // make sure there are no gaps between different tessellated distances, by averaging the edges out.
                f.edge[0] = (edge1 + edge2) / 2;
                f.edge[1] = (edge2 + edge0) / 2;
                f.edge[2] = (edge0 + edge1) / 2;
                f.inside = (edge0 + edge1 + edge2) / 3;
                return f;
            }

            // The vertex shader definition with properties defined in the Varyings 
            // structure. The type of the vert function must match the type (struct)
            // that it returns.
            Varyings vert(Attributes IN)
            {
                // Declaring the output object (OUT) with the Varyings struct.
                Varyings OUT;

                // VertexPositionInputs contains position in multiple spaces (world, view, homogeneous clip space, ndc)
                // Unity compiler will strip all unused references (say you don't use view space).
                // Therefore there is more flexibility at no additional cost with this struct.
                VertexPositionInputs vertexPositionInput = GetVertexPositionInputs(IN.vertex.xyz);

                // The TransformObjectToHClip function transforms vertex positions
                // from object space to homogenous clip space.
                OUT.positionCS = vertexPositionInput.positionCS;
                OUT.color = IN.color;
                OUT.normal = IN.normal;
                OUT.normalWS = TransformObjectToWorldNormal(IN.normal);
                OUT.positionWS = vertexPositionInput.positionWS;
                

                // regular unity fog
#if _UnityFogEnable
                OUT.uv_fog = float3(IN.uv, ComputeFogFactor(OUT.positionCS.z));
#else
                OUT.uv_fog = float3(IN.uv, 0);
#endif
                return OUT;
            }

            // Step 4)
            // prepare vertices & triangles data for Geomertry Program
            // In order work, send data to org vertex program.
            [UNITY_domain("tri")]
            Varyings domain(TessellationFactors factors, OutputPatch<ControlPoint, 3> patch, float3 barycentricCoordinates : SV_DomainLocation)
            {
                Attributes v;

                #define DomainPos(fieldName) v.fieldName = \
				patch[0].fieldName * barycentricCoordinates.x + \
				patch[1].fieldName * barycentricCoordinates.y + \
				patch[2].fieldName * barycentricCoordinates.z;

                DomainPos(vertex)
                DomainPos(color)
                DomainPos(normal)
                DomainPos(uv)
                DomainPos(lightmapuv)

                return vert(v);
            }
            
            // The fragment shader definition.            
            float4 frag(Varyings IN) : SV_Target
            {
                // Defining the color variable and returning it.
                float2 uv1 = TRANSFORM_TEX(IN.uv_fog.xy, _MainTex);
                float4 col = _Color * tex2D(_MainTex, uv1);
                
#if _SSSEnable
                float2 uv2 = TRANSFORM_TEX(IN.uv_fog.xy, _LightAmbient);
                float2 uv3 = TRANSFORM_TEX(IN.uv_fog.xy, _LightThickness);
                float4 ambient = tex2D(_LightAmbient, uv2);
                float4 thickness = tex2D(_LightThickness, uv3);

                float3 lightDir = _MainLightPosition.xyz;
				float3 viewDir = normalize(_WorldSpaceCameraPos - IN.positionWS);
				float3 lightColor = _MainLightColor.rgb;

                float3 L = lightDir + IN.normal * _LightDistortion;
                float fLTDot = pow(saturate(dot(viewDir, -L)), _LightPower) * _LightScale;
                float3 fLT = (_LightAttenuation * (fLTDot + ambient) * thickness).xyz;
                
                col.rgb += _Color_DiffuseAlbedo.rgb * _Color_LightDiffuse.rgb * fLT;
                
                // col = ambient + thickness / 2;
#endif

#if _UnityFogEnable
                // Mix the pixel color with fogColor. You can optionaly use MixFogColor to override the fogColor
                // with a custom one.
                col.rgb = MixFog(col.rgb, IN.uv_fog.z);
#endif
                return col;
            }
            ENDHLSL
        }
    }
}